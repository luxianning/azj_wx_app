<?php
/**
 * 微信公众平台 PHP SDK 示例文件
 *
 * @author NetPuter <netputer@gmail.com>
 */

define('CY_HOME', __DIR__);

include CY_HOME.'/src/init.php';

$wechat = new CY_Bootstrap('izijia', TRUE);
$wechat->run();
