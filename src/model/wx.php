<?php

class CY_Model_WX
{
	protected $token;
    protected $jsApiTicket;
	protected $appid;
	protected $key;
	protected $db;
    protected $_time;

	function __construct()
	{
		$this->appid = $_ENV['config']['wx_appid'];
		$this->key   = $_ENV['config']['wx_key'  ];

		$this->db    = new CY_Util_MySQL();
		$this->init();
	}

	function init()
	{
        $this->_time = time();
        $sql = "SELECT token, ticket, expired_time FROM tokens WHERE expired_time > ".$this->_time." ORDER BY create_time DESC LIMIT 1";
		$r = $this->db->query($sql);
		if($r['errno'] === 0 && isset($r['data'][0]))
		{
            $this->token = $r['data'][0]['token'];
            $this->jsApiTicket = $r['data'][0]['ticket'];
		}

		if(empty($this->token))
		{
			$r = $this->get_token();
			if($r['errno'] === 0)
			{
				$val = $r['data'];
				$this->set_token($val);
			}
		}
	}

    public function getAccessToken() {
        return $this->token;
    }

    public function getJsApiTicket() {
        return $this->jsApiTicket;
    }

	function set_token($val)
	{
		$this->token = $val['access_token'];
        $this->jsApiTicket = $val['jsapi_ticket'];
        $sql= "INSERT INTO tokens SET token='{$this->token}', ticket='{$this->jsApiTicket}', expired_time='".($this->_time + 7000)."', create_time='".$this->_time."'";
		//$val['time'] = $_SERVER['REQUEST_TIME'];
		//$val = json_encode($val);
		//$sql = 'REPLACE INTO `session` SET `value`="'.$this->db->escape_string($val).'", `key`="wx_token"';
		$sr = $this->db->query($sql);
		if($sr['errno'] !== 0)
		{
			cy_log(CYE_ERROR, $sql." Error!");
			return cy_dt(-1, 'db error.');
		}

		// TODO write log on ERROR.

		return cy_dt(0);
	}

	function get_token()
	{
		$url   = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$this->appid.'&secret='.$this->key;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url);

		$data = json_decode($text, true);
		if(isset($data['access_token']))
		{
            $accessToken = $data['access_token'];
            $jsapiurl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
		    $jsapitext = $curl->fetch($jsapiurl);
		    $jsapidata = json_decode($jsapitext, true);
            $data['jsapi_ticket'] = $jsapidata['ticket'];
			return cy_dt(0, $data);
		}

		cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
		return cy_dt(-1);
	}

	function new_token()
	{
		$r = $this->get_token();
		if($r['errno'] === 0)
		{
			$this->set_token($r['data']);
		}
	}

	function user_info($openid)
	{
        if (empty($openid)) {
            return cy_dt(-1, 'openid不能为空');
        }
again:
		$url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$this->token.'&openid='.$openid;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url);
		$data = json_decode($text, true);
		$errno = isset($data['errcode']) ? $data['errcode'] : 0;
		if($errno == 40001 && empty($is_again))
		{
			$is_again = 1;
			$this->new_token();
			goto again;
		}

		return cy_dt($errno, $data);
	}

	function user_info1($openid, &$access_token, &$refresh_token)
	{
        if (empty($openid)) {
            return cy_dt(-1, 'openid不能为空');
        }
		$r = $this->refresh_token($openid, $refresh_token);
		if($r['errno'] !== 0)
		{
			cy_log(CYE_ERROR, 'fetch url refresh_token error, '.json_encode($data));
			return $r;
		}

		$access_token = $r['data']['access_token'];
		$refresh_token = $r['data']['refresh_token'];
		$url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token
			.'&openid='.$openid.'&lang=zh_CN';

		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url'.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		unset($data['privilege']);
		return cy_dt(0, $data);	
	}

	function refresh_token($openid, $refresh_token)
	{
		$url = 'https://api.weixin.qq.com/sns/oauth2/refresh_token?appid='.$this->appid
			.'&grant_type=refresh_token&refresh_token='.$refresh_token;

		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
	}

	function oauth($code)
	{
		$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->appid.'&secret='.$this->key
			.'&code='.$code.'&grant_type=authorization_code';

		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
	}

	function qr_tiket($id)
	{
		$url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$this->token;
        $args = '{"action_name": "QR_SCENE", "action_info": {"scene": {"scene_id": '.$id.'}}}';

		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $args]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
	}

    // 永久二维码
	public function qrStrTiket($channel)
	{
		$url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$this->token;
        $args = '{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str":"'.$channel.'"}}}';

		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $args]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
	}

    // 发送提醒消息
    public function sendCustomMessage($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
    }

    // 生成菜单
    public function setMenu($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
    }

    // 上传图片，3天有效期
    public function getMediaId($type, $postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=".$this->token."&type=".$type;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data['media_id']);
    }

    // 预览群发消息
    public function preview($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt($data['errcode'], $data['msg_id']);
    }

    // 上传群发图文消息到素材库
    public function uploadNews($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data['media_id']);
    }

    // 根据OpenID列表群发
    public function sendOpenidsMessage($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt($data['errcode'], $data['msg_id']);
    }

    // 群发消息
    public function sendAllMessage($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt($data['errcode'], $data['msg_id']);
    }

    // 给用户返回带参数二维码
    public function sendQrcode($mediaId, $toUser) {
        $postData = array(
            "touser" => $toUser,
            "msgtype" => "image",
            "image" => array(
                "media_id" => $mediaId
            )
        );
        return $this->sendCustomMessage(CY_Util_Tools::jsonEncodeEx($postData));
    }

    public function getMediaInfo($postData) {
        $url = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=".$this->token;
        //$url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=".$this->token;
		$curl = new CY_Util_Curl();
		$text = $curl->fetch($url, 'POST', ['data' => $postData]);
		$data = json_decode($text, true);
		if(isset($data['errcode']))
		{
			cy_log(CYE_ERROR, 'fetch url '.$url.' error, '.json_encode($data));
			return cy_dt($data['errcode'], $data['errmsg']);
		}

		return cy_dt(0, $data);
    }

}

