<?php

class CY_Model_Activity
{
	protected $db;
    protected $time;

	public function __construct()
	{
		$this->db = new CY_Util_MySQL();
        $this->time = date('Y-m-d H:i:s');
	}

    public function get($pair)
    {
        $rs = $this->db->get('activity', $pair);
		if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
            $items = array();
            foreach ($rs['data'] as $r) {
                $this->formatActivity($r);
                $items[] = $r;
            }
            $rs['data'] = $items;
        }
        return $rs;
    }

    public function updateStatis($id) {
        $update = "UPDATE activity SET visits = visits + 1 WHERE id={$id}";
        $this->db->query($update);
    }

    public function getActivityByKeyword($key) {
        $items = array();
        $query = "SELECT id, title, summary, thumb FROM activity WHERE keyword='".$this->db->escape_string($key)."' AND status=1 ORDER BY start_time DESC LIMIT 8";
		$rs = $this->db->query($query);
		if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
            foreach ($rs['data'] as $r) {
                $items[] = array(
                    'title' => $r['title'],
                    'desc' => $r['summary'],
                    'picurl' => $r['thumb'],
                    'url' => $_ENV['config']['site']."/index.php?m=activity&f=detail&id=".$r['id']
                );
            }
        }
        return $items;
    }

    public function getActivity($limit = 1, $offset = 0, $type = 0) {
        $items = array();
        $query = "SELECT *, date(start_time) start_date, date(end_time) end_date FROM activity WHERE status=1";
        if ($type == 1) { // 未结束
            $query .= " AND end_time > '".$this->time."'";
        } elseif ($type == 2) { // 已结束
            $query .= " AND end_time < '".$this->time."'";
        }
        $query .= " ORDER BY start_time DESC LIMIT $offset, $limit";
		$rs = $this->db->query($query);
		if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
            foreach ($rs['data'] as $r) {
                $this->formatActivity($r);
                $items[] = $r;
            }
        }
        return $items;
    }

    public function checkJoinedByUidAndAid($uid, $aid) {
        $query = "SELECT id FROM activity_user WHERE activity_id=$aid AND user_id=$uid";
		return $this->db->query($query);
    }

    public function getActivityUsers($aid) {
        $query = "SELECT u.nickname, u.headimgurl, au.help_times, au.user_remark FROM activity_user au LEFT JOIN user u ON au.user_id=u.id WHERE au.activity_id=".$this->db->escape_string($aid)." ORDER BY au.help_times DESC LIMIT 50";
		return $this->db->query($query);
    }

    public function getActivityAwards($aid) {
        $query = "SELECT title, name, img, total, `describe` FROM award WHERE activity_id=".$this->db->escape_string($aid)." AND status=0 ORDER BY id";
		return $this->db->query($query);
    }

    public function getActivityUser($auId) {
        $query = "SELECT a.invite_img, u.openid, u.nickname, u.headimgurl, u.imglocal, au.id, au.help_times, au.activity_id, au.user_id, au.media_id, au.media_expired, au.imgurl,"
            ."(SELECT IF(MIN(help_times) >= 0, MIN(help_times), au.help_times) FROM activity_user WHERE activity_id=au.activity_id AND help_times > au.help_times) minTimes, "
            ."(SELECT COUNT(*) FROM activity_user WHERE activity_id=au.activity_id AND help_times > au.help_times) + 1 rank, "
            ." (SELECT COUNT(*) FROM activity_user_awards aua WHERE aua.activity_user_id=au.id) win"
            ." FROM activity_user au LEFT JOIN activity a ON au.activity_id=a.id LEFT JOIN user u ON au.user_id=u.id WHERE au.id='".$this->db->escape_string($auId)."'";
		return $this->db->query($query);
    }

    public function getHelpUsers($auId) {
        $query = "SELECT u.nickname, u.headimgurl FROM activity_user_invite aui LEFT JOIN user u ON aui.invited_user_id=u.id WHERE aui.activity_user_id=".$this->db->escape_string($auId)." ORDER BY aui.id DESC LIMIT 5";
		return $this->db->query($query);
    }

    public function checkValid($activityId) {
        $ret= $this->get(['id' => $activityId]);
        if ($ret['errno'] != 0 || empty($ret['data'][0])) {
            return cy_dt(-1, '活动不存在');
        }
        $activity = $ret['data'][0];
        if ($activity['status'] != 1) {
            return cy_dt(-1, '活动已失效');
        }
        $timeSec = strtotime($this->time);
        $startSec = strtotime($activity['start_time']);
        $endSec = strtotime($activity['end_time']);
        if ($timeSec < $startSec || $timeSec > $endSec) {
            return cy_dt(-1, '活动已过期');
        }
        return cy_dt(0);
    }

    // 参加活动，同时生成邀请卡
    public function join($activityId, $userId, $openid) {
        $check = $this->checkValid($activityId);
        if ($check['errno'] != 0) {
            return $check;
        }
        $ret = $this->checkJoinedByUidAndAid($userId, $activityId);
        if ($ret['errno'] == 0 && !empty($ret['data'][0])) {
            $auid = $ret['data'][0]['id'];
            return cy_dt(0, $auid);
        } else {
            $data['activity_id'] = $activityId;
            $data['user_id'] = $userId;
            $data['apply_time'] = $this->time;
            $r = $this->db->insert('activity_user', $data);
            if ($r['errno'] === 0) {
                // 生成二维码以及存储
                //$media = $this->getImageMediaId($r['data']['insert_id']);
                // 更新参加人数
                $update = "UPDATE activity SET apply_num = apply_num + 1 WHERE id={$activityId}";
                $this->db->query($update);
                return cy_dt(0, $r['data']['insert_id']);
            } else {
                return cy_dt(-1, '服务器异常');
            }
        }
    }

    public function sendJoinSuccMessage($aid, $auId, $toUser) {
        $r = $this->get(['id' => $aid]);
        if ($r['errno'] === 0) {
            $activity = $r['data'][0];
            $data = array(
                "touser" => $toUser,
                "msgtype" => "news",
                "news" => array(
                    "articles" => array(
                        array(
                            "title" => "参加 ".$activity['title']." 成功！",
                            "description" => $activity['summary'],
                            "url" => $_ENV['config']['site']."/index.php?m=activity&f=joinSuccess&id=".$auId,
                            "picurl" => $_ENV['config']['imgsite'].$activity['thumb']
                        )
                    )
                )
            );
            $wm = new CY_Model_WX();
            $wm->sendCustomMessage(CY_Util_Tools::jsonEncodeEx($data));
        }
    }

    public function sendHelpSuccMessage($aid, $auId, $toUser) {
        $r = $this->get(['id' => $aid]);
        if ($r['errno'] === 0) {
            $activity = $r['data'][0];
            $data = array(
                "touser" => $toUser,
                "msgtype" => "news",
                "news" => array(
                    "articles" => array(
                        array(
                            "title" => "恭喜您，为好友助力成功！",
                            "description" => "你也来参与一下活动赢取大奖吧，点击参与活动",
                            "url" => $_ENV['config']['site']."/index.php?m=activity&f=detail&id=".$aid,
                            "picurl" => $_ENV['config']['imgsite'].$activity['thumb']
                        )
                    )
                )
            );
            $wm = new CY_Model_WX();
            $wm->sendCustomMessage(CY_Util_Tools::jsonEncodeEx($data));
        }
    }

    // 
    public function help($activityId, $activityUserId, $invitedUserId, $new = 0) {
        $check = $this->checkValid($activityId);
        if ($check['errno'] != 0) {
            return $check;
        }
        $query = "INSERT INTO activity_user_invite SET activity_user_id='{$activityUserId}', invited_user_id='$invitedUserId', "
            ."invite_time='{$this->time}', isnew={$new}";
        $ret = $this->db->query($query);
        if ($ret['errno'] == 0) {
            $update = "UPDATE activity_user SET help_times = help_times + 1 ";
            if ($new == 1) {
                $update .= ", subscribe = subscribe + 1";
            }
            $update .= " WHERE id={$activityUserId}";
            $this->db->query($update);
            if ($new == 1) {// 新增一个粉丝
                $query = "UPDATE activity SET new_fans_num=new_fans_num+1 WHERE id=$activityId";
                $this->db->query($query);
            }
        }
        return $ret;
    }

    public function getMyJoins($userId, $stt = 0, $oft = 50) {
        $ret = array();
        if (empty($userId)) {
            return $ret;
        }
        $query = "SELECT au.id, au.activity_id, a.title, a.short_title, a.summary, a.thumb, a.share_logo, date(a.start_time) start_date,"
            ." date(a.end_time) end_date, au.help_times," 
            ." (SELECT COUNT(*) FROM activity_user au2 WHERE au2.activity_id=au.activity_id AND au2.help_times > au.help_times) + 1 rank, "
            ." (SELECT COUNT(*) FROM activity_user_awards aua WHERE aua.activity_user_id=au.id) win"
            ." FROM activity_user au LEFT JOIN activity a ON au.activity_id=a.id WHERE au.user_id=".$this->db->escape_string($userId)." ORDER BY a.start_time DESC LIMIT $stt, $oft";
		$rs = $this->db->query($query);
		if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
            foreach ($rs['data'] as $r) {
                $this->formatActivity($r);
                $ret[] = $r;
            }
        }
        return $ret;
    }

    public function getUserAwardsInfo($aid, $uid) {
        $query = "SELECT aua.realname, aua.mobile, aua.address, aua.idno, a.title, a.start_time, a.end_time, aua.id, a.thumb, a.share_logo, aua.activity_id, ad.title award_title, aua.create_time FROM activity_user_awards aua LEFT JOIN activity a ON aua.activity_id=a.id LEFT JOIN award ad ON aua.award_id=ad.id WHERE aua.activity_id=$aid AND aua.user_id=$uid";
        return $this->db->query($query);
    }

    public function updateUserAwardsInfo($aid, $uid, $datas) {
        return $this->db->update('activity_user_awards', $datas, "activity_id='$aid' AND user_id='$uid'");
    }

    private function formatActivity(&$r) {
        $r['titleListShow'] = mb_substr($r['title'], 0, 28, 'utf-8');
        if (!isset($r['start_date'])) {
            $r['start_date'] = date('Y-m-d', strtotime($r['start_time']));
        }
        if (!isset($r['end_date'])) {
            $r['end_date'] = date('Y-m-d', strtotime($r['end_time']));
        }
        if (strtotime($this->time) < strtotime($r['start_date'])) {
            $r['step'] = 0; // 未开始
        } elseif (strtotime($this->time) <= strtotime($r['end_date']) + 86399) {
            $r['step'] = 1; // 进行中
        } else {
            $r['step'] = 2; // 已结束
        }
    }

    // 获取邀请卡二维码图片
    public function getImageMediaId($auId) {
        $r = $this->getActivityUser($auId);
        if ($r['errno'] == 0 && !empty($r['data'])) {
            $data = $r['data'][0];
            if (time() < strtotime($data['media_expired'])) {
                $mediaData = array(
                    'media_id' => $data['media_id'],
                    'imgurlLocal' => $_ENV['config']['imgsite']."/qrcode".$data['imgurl']
                );
                return cy_dt(0, $mediaData);
            } else {
                //include CY_LIB_PATH.'/3rd/ThinkImage/ThinkImage.class.php';
                //include CY_LIB_PATH.'/3rd/ThinkImage/Driver/ImageGd.class.php';
                //include CY_LIB_PATH.'/3rd/ThinkImage/Driver/ImageImagick.class.php';
                include_once('./src/3rd/phpqrcode.php'); 
                $wm = new CY_Model_WX();
                //$qrRet= $wm->qr_tiket($auId);
                //$ticket = $qrRet['data']['ticket'];
                //$url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$ticket;
                $url = $_ENV['config']['site']."/index.php?m=activity&f=help&id=".$auId;
                //file_put_contents($fileTmp, file_get_contents($url));
                $md5 = md5("qrcode_complex_".$auId.".jpg");
                $path = "/".substr($md5, 0, 2)."/".substr($md5, 2, 2);
                $file = $_ENV['config']['uploadpath']."/qrcode".$path."/".$md5.".jpg";
                $imgPath = $path."/".$md5.".jpg";
                if (!file_exists($file)) {
                    if (!file_exists($_ENV['config']['uploadpath']."/qrcode".$path)) {
                        mkdir($_ENV['config']['uploadpath']."/qrcode".$path, 0755, true);
                    }
                    $fileTmp = "/tmp/qrcode_tmp_".$auId.".png";
                    $fileTmp1 = "/tmp/qrcode_tmp1_".$auId.".png";
                    if (!file_exists($fileTmp)) {
                        QRcode::png($url, $fileTmp, 'L', 4, 2);
                    }
                    //$bgImg = CY_HOME.'/static/files/card_background.gif';
                    $url = $data['invite_img'];
                    $bgFile = str_replace($_ENV['config']['imgsite'], '', $url);
                    $bgImg = $_ENV['config']['uploadpath'].$bgFile;
                    //$bgImg = "/tmp/qrcode_bg_".$data['activity_id'].".jpg";
                    //if (!file_exists($bgImg)) {
                    //    file_put_contents($bgImg, file_get_contents($url));
                    //}
                    //$faceUrl = $data['headimgurl']."132";
                    //$faceImg = "/tmp/qrcode_face_".$auId."_".$data['user_id'].".jpg";
                    //if (!file_exists($faceImg)) {
                    //    file_put_contents($faceImg, file_get_contents($faceUrl));
                    //}
                    $faceImg = $_ENV['config']['uploadpath']."/qrcode".$data['imglocal'];
                    //$font = CY_HOME.'/static/files/simsun.ttc';
                    $font = CY_HOME.'/static/files/msyh.ttf';
                    //$len = strlen($data['nickname']);
                    //$x = $len / 2 * 6;
                    system("convert $bgImg $fileTmp -gravity north -geometry 330x330+0+700 -composite -fill black -font $font -weight Bold -pointsize 24 -gravity north -draw \"text 0,200 '".$data['nickname']." 的邀请卡'\" $fileTmp1", $out);
                    system("convert $fileTmp1 $faceImg -gravity north -geometry 120x120+0+60 -composite $file", $out);
                    system("rm $fileTmp $fileTmp1");
                }
                $fields['f'] = '@'.$file;
                $media = $wm->getMediaId('image', $fields);
                $mediaId = $media['data'];
                // 更新库中信息
                if (!empty($mediaId)) {
                    $update['media_id'] = $mediaId;
                    $update['imgurl'] = $imgPath;
                    $update['media_expired'] = date('Y-m-d H:i:s', strtotime($this->time) + 86400 * 3);
                    $this->db->update('activity_user', $update, "id=".$auId);
                    $mediaData = array(
                        'media_id' => $mediaId,
                        'imgurlLocal' => $_ENV['config']['imgsite']."/qrcode".$imgPath
                    );
                    return cy_dt(0, $mediaData);
                }
            }
        }
        return cy_dt(-1, '获取信息失败！');
    }

    public function pushMessage($aid, $backend = 0) {
        $ret= $this->get(['id' => $aid]);
        if ($ret['errno'] != 0 || empty($ret['data'][0])) {
            return cy_dt(-1, '活动不存在');
        }
        $activity = $ret['data'][0];
        $imgurl = $activity['thumb'];
        $imgFile = str_replace($_ENV['config']['imgsite'], '', $imgurl);
        $imgPath = $_ENV['config']['uploadpath'].$imgFile;
        $fields['f'] = '@'.$imgPath;
        $wm = new CY_Model_WX();
        $media = $wm->getMediaId('image', $fields);
        $mediaId = $media['data'];
        $datas = array(
            "articles" => array(
                array(
                    "thumb_media_id" => $mediaId,
                    "author" => '爱自驾联盟',
                    "title" => $activity['short_title'],
                    "content_source_url" => $_ENV['config']['site']."/index.php?m=activity&f=detail&id=".$activity['id'],
                    "content" => $activity['content'],
                    "digest" => $activity['summary'],
                    "show_cover_pic" => 1
                )
            )
        );
        $imgUpload = $wm->uploadNews(CY_Util_Tools::jsonEncodeEx($datas));
        $datas = array(
            "mpnews" => array(
                "media_id" => $imgUpload['data']
            ),
            "msgtype" => "mpnews"
        );
        if ($backend == 1) {
            $datas["filter"] = array(
                "is_to_all" => true,
                "group_id" => 1
            );
            $pushRet = $wm->sendAllMessage(CY_Util_Tools::jsonEncodeEx($datas));
        } else {
            $datas['touser'] = $_COOKIE['wxid'];
            $pushRet = $wm->preview(CY_Util_Tools::jsonEncodeEx($datas));
        }
        return $pushRet;
    }

    public function getUserAwards($aid) {
        $query = "SELECT u.openid, a.title, a.name FROM activity_user_awards aua LEFT JOIN award a ON aua.award_id=a.id LEFT JOIN user u ON aua.user_id=u.id WHERE aua.activity_id='$aid'";
        return $this->db->query($query);
    }

    public function pushAwardsMessage($aid, $backend = 0) {
        $ret= $this->get(['id' => $aid]);
        if ($ret['errno'] != 0 || empty($ret['data'][0])) {
            return cy_dt(-1, '活动不存在');
        }
        $activity = $ret['data'][0];
        $wm = new CY_Model_WX();
        // 给所有中奖的人发送信息
        $awards = $this->getUserAwards($aid);
        if ($awards['errno'] == 0 && !empty($awards['data'])) {
            foreach ($awards['data'] as $award) {
                if (empty($award['openid'])) {
                    continue;
                }
                $datas = array();
                $desc = "您参与的".$activity['title']."活动，获得".$award['title']."：".$award['name']."。请完善联系方式领取奖励。";
                $datas = array(
                    "touser" => $award['openid'],
                    "msgtype" => "news",
                    "news" => array(
                        "articles" => array(
                            array(
                                "title" => "恭喜您中了".$award['title']."！",
                                "description" => $desc,
                                "url" => $_ENV['config']['site']."/index.php?m=activity&f=myaward&id=".$activity['id'],
                                "picurl" => $_ENV['config']['imgsite'].$activity['thumb']
                            )
                        )
                    )
                );
                //$wm = new CY_Model_WX();
                if ($backend != 1) {
                    $datas['touser'] = $_COOKIE['wxid'];
                }
                $pushRet = $wm->sendCustomMessage(CY_Util_Tools::jsonEncodeEx($datas));
            }
        }
        return cy_dt(0);
    }

    // 获取需要发送助力数提醒的活动ID
    public function getNoticeActivities($days = 3) {
        $ids = array();
        $query = "SELECT id, TO_DAYS(NOW()) - TO_DAYS(start_time) days FROM activity WHERE status=1 AND"
            ." start_time < '".$this->time."' AND end_time > '".$this->time."'";
		$rs = $this->db->query($query);
		if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
            foreach ($rs['data'] as $r) {
                if ($r['days'] > 0 && $r['days'] % 3 == 0) {
                    $ids[] = $r['id'];
                }
            }
        }
        return $ids;
    }

    public function noticeUserHelps($days = 3) {
        $ids = $this->getNoticeActivities($days);
        foreach ($ids as $aid) {
            $stt = 0;
            $oft = 10000;
            while (1) {
                $query = "SELECT au.id, au.user_id, au.help_times, a.title, a.thumb, u.openid, "
                    ."(SELECT COUNT(*) FROM activity_user WHERE activity_id=au.activity_id AND help_times > au.help_times) + 1 rank "
                    ." FROM activity_user au LEFT JOIN activity a ON au.activity_id=a.id "
                    ." LEFT JOIN user u ON au.user_id=u.id WHERE au.activity_id=$aid ORDER BY au.id LIMIT $stt, $oft";
		        $rs = $this->db->query($query);
		        if ($rs['errno'] === 0 && !empty($rs['data'][0])) {
                    foreach ($rs['data'] as $r) {
                        $this->doNoticeHelps($r);
                    }
                } else {
                    break;
                }
                $stt += $oft;
            }
        }
        return cy_dt(0);
    }

    public function doNoticeHelps($data) {
        $openid = $data['openid'];
        $aid = $data['id'];
        $title = $data['title'];
        $thumb = $_ENV['config']['imgsite'].$data['thumb'];
        $helpTimes = $data['help_times'];
        $rank = $data['rank'];
        $desc = <<<HTML
$title
再接再厉，继续邀请好友为你助力吧！
HTML;
        $data = array(
            "touser" => $openid,
            "msgtype" => "news",
            "news" => array(
                "articles" => array(
                    array(
                        "title" => "您目前排名第${rank}位，已有${helpTimes}位好友为您助力",
                        "description" => $desc,
                        "url" => $_ENV['config']['site']."/index.php?m=activity&f=joinSuccess&id=".$aid,
                        "picurl" => $thumb
                    )
                )
            )
        );
        $wm = new CY_Model_WX();
        $wm->sendCustomMessage(CY_Util_Tools::jsonEncodeEx($data));
    }
}
