<?php

class CY_Model_Users
{
    protected $db;

    function __construct()
    {
        $this->db = new CY_Util_MySQL();
    }

    function init($openid, $wx_model)
    {
        $r = $this->get(['openid' => $openid]);
        if($r['errno'] === 0)
        {
            if(empty($r['data']))
            {
                $wx_r = $wx_model->user_info($openid);
                if($wx_r['errno'] === 0)
                {
                    $data = [];
                    unset($wx_r['data']['openid']);

                    $data['openid'] = $openid;
                    $data['unionid'] = $wx_r['data']['unionid'];
                    $data['nickname'] = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $wx_r['data']['nickname']);
                    $data['sex'] = $wx_r['data']['sex'];
                    $data['subscribe'] = $wx_r['data']['subscribe'];
                    $data['city'] = $wx_r['data']['city'];
                    $data['country'] = $wx_r['data']['country'];
                    $data['province'] = $wx_r['data']['province'];
                    $data['headimgurl'] = substr($wx_r['data']['headimgurl'], 0, -1);
                    $data['subscribe_time'] = $wx_r['data']['subscribe_time'];
                    $data['remark'] = $wx_r['data']['remark'];
                    $data['groupid'] = $wx_r['data']['groupid'];

                    $this->set($data);
                    // TODO write log on ERROR.  
                }
            }
            else if(isset($r['data'][0]))
            {
                $rd = $r['data'][0];
                $access_token   = $rd['access_token' ];
                $refresh_token  = $rd['refresh_token'];
                $oauth_time = date('Y-m-d H:i:s');
                $wx_r = $wx_model->user_info1($openid, $access_token, $refresh_token);
                if($wx_r['errno'] === 0)
                {
                    $data = [];
                    $data['openid'] = $openid;
                    $data['unionid'] = $wx_r['data']['unionid'];
                    $data['access_token'] = $access_token;
                    $data['refresh_token'] = $refresh_token;
                    $data['oauth_time'] = $oauth_time;
                    $data['nickname'] = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $wx_r['data']['nickname']);
                    $data['sex'] = $wx_r['data']['sex'];
                    //$data['subscribe'] = $wx_r['data']['subscribe'];
                    $data['city'] = $wx_r['data']['city'];
                    $data['country'] = $wx_r['data']['country'];
                    $data['province'] = $wx_r['data']['province'];
                    $data['headimgurl'] = substr($wx_r['data']['headimgurl'], 0, -1);
                    //$data['subscribe_time'] = $wx_r['data']['subscribe_time'];
                    //$data['remark'] = $wx_r['data']['remark'];
                    //$data['groupid'] = $wx_r['data']['groupid'];

                    $this->update(['openid' => $openid], $data);
                }
            }
        }
    }

    function set($info)
    {
        $cond = 'ON DUPLICATE KEY UPDATE `subscribe`=VALUES(`subscribe`)';
        $r = $this->db->insert('user', $info, $cond);
        return $r;
    }

    function get($pair)
    {
        $r = $this->db->get('user', $pair);
        return $r;
    }


    function update($pair, $data)
    {
        if(empty($pair['openid']))
        {
            return cy_dt(-1);
        }

        $cond = "`openid`='".$this->db->escape_string($pair['openid'])."'";
        $r = $this->db->update('user', $data, $cond);
        return $r;
    }

    public function updateUserInfo($openid, $inviteUser = "") {
        $ret = array('new' => 0, 'user' => null);
        $wm = new CY_Model_WX();
        $ret['new'] = 0; // 默认之前已经关注过，1为新关注，成功邀请
        $wx_r = $wm->user_info($openid);
        if($wx_r['errno'] === 0) {
            $data = [];
            $data['unionid'] = $wx_r['data']['unionid'];
            $data['nickname'] = preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $wx_r['data']['nickname']);
            $data['sex'] = $wx_r['data']['sex'];
            $data['subscribe'] = $wx_r['data']['subscribe'];
            $data['city'] = $wx_r['data']['city'];
            $data['country'] = $wx_r['data']['country'];
            $data['province'] = $wx_r['data']['province'];
            $data['headimgurl'] = substr($wx_r['data']['headimgurl'], 0, -1);
            // 本地化头像图片
            $md5 = md5($wx_r['data']['headimgurl']);
            $path = "/".substr($md5, 0, 2)."/".substr($md5, 2, 2);
            $file = $_ENV['config']['uploadpath']."/qrcode".$path."/".$md5.".jpg";
            if (!file_exists($file)) {
                if (!file_exists($_ENV['config']['uploadpath']."/qrcode".$path)) {
                    mkdir($_ENV['config']['uploadpath']."/qrcode".$path, 0755, true);
                }
                file_put_contents($file, file_get_contents($wx_r['data']['headimgurl']));
            }
            $data['imglocal'] = $path."/".$md5.".jpg";
            
            $data['subscribe_time'] = $wx_r['data']['subscribe_time'];
            $data['remark'] = $wx_r['data']['remark'];
            $data['groupid'] = $wx_r['data']['groupid'];

            $r = $this->get(['openid' => $openid]);
            if($r['errno'] === 0 && !empty($r['data'])) {
                $user = $r['data'][0];
                if ($user['subscribe'] == 0 && $user['nickname'] == '' && $user['invite_openid'] == '') {
                    $data['invite_openid'] = $inviteUser;
                    $ret['new'] = 1;
                }
                $this->update(['openid' => $openid], $data);
            } else {
                $data['openid'] = $openid;
                $data['invite_openid'] = $inviteUser;
                $ret['new'] = 1;
                $this->set($data);
            }
            $userRet = $this->get(['openid' => $openid]);
            $ret['user'] = $userRet['data'][0];
        }
        return $ret;
    }

}

?>
