<?php

class CY_Model_Channel
{
	protected $db;
    protected $time;

	public function __construct()
	{
		$this->db = new CY_Util_MySQL();
        $this->time = date('Y-m-d H:i:s');
	}

    public function addChannel($name, $str)
    {
        $channel = "QR_".$str."_".time();
        $wx = new CY_Model_WX();
        $ret = $wx->qrStrTiket($channel);
        if ($ret['errno'] == 0) {
            $ticket = $ret['data']['ticket'];
            $url = $ret['data']['url'];
            $img = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$ticket;
            $query = "INSERT INTO channel_stat SET name='".$this->db->escape_string($name)."', channel='".$this->db->escape_string($channel)."', ticket='$ticket', url='$url', img='".$this->db->escape_string($img)."'";
            $dbRet = $this->db->query($query);
            if ($dbRet['errno'] == 0) {
                return cy_dt(0, '添加新渠道成功');
            }
        }
        return cy_dt(-1, '添加新渠道失败');
    }

    public function addScan($channel, $openid) {
        $query = "INSERT INTO channel_stat SET channel='$channel', scan = 1, subscribe = 0"
            ." ON DUPLICATE KEY UPDATE scan = scan + 1";
        $this->db->query($query);
        $this->addDetail($channel, $openid, 0);
    }

    public function addSubscribe($channel, $openid) {
        $affectRow = $this->addDetail($channel, $openid, 1);
        if ($affectRow) {
            $query = "INSERT INTO channel_stat SET channel='$channel', scan = 0, subscribe = 1"
                ." ON DUPLICATE KEY UPDATE subscribe = subscribe + 1";
            $this->db->query($query);
        }
    }

    public function addDetail($channel, $openid, $type = 0) {
        $query = "INSERT IGNORE INTO channel_detail SET channel='$channel', type=$type, openid='$openid', create_time = '".$this->time."'";
        $ret = $this->db->query($query);
	//file_put_contents('/opt/lampp/htdocs/wx-app/tmp.log', $ret['data']['affected_rows'], FILE_APPEND | LOCK_EX);	
        if ($ret['error'] == 0 && $ret['data']['affected_rows'] == 1) {
            return true;
        }
        return false;
    }
}
