<?php

if (!defined('MYSQL_OPT_READ_TIMEOUT'))
{
    define('MYSQL_OPT_READ_TIMEOUT',  11);
}

if (!defined('MYSQL_OPT_WRITE_TIMEOUT'))
{
    define('MYSQL_OPT_WRITE_TIMEOUT', 12);
}

/**
 * mysql operation class
 **/
class CY_Util_MySQL
{
    protected $hd    = array();
    protected $tasks = array();
    protected $mysqlc;

    protected $default_config = NULL;

    protected function config_key($config)
    {
        if(!$config)
        {
            return 'default';
        }
        
        return md5(implode($config[0]));

    }

    function __construct($config = NULL)
    {
        if($config)
        {
            $this->default_config = $config;
        }

        $this->mysqlc = new CY_Util_Mysqlc();
    }

    function add($key, $c, $config = NULL)
    {
        if(!$config && $this->default_config)
        {
            $config = $this->default_config;
        }
        $this->tasks[$key] = ['c' => $c, 'config' => $config, 'config_key' => $this->config_key($config)];
    }



    /**
     * mGet multi_query
     *
     * $options
     * $options['async']  if async is set, the function will return every 100ms.
     */
    function mGet($options = [])
    {
        $t1    = microtime(true);
        $cycle = isset($options['cycle']) ? $options['cycle'] : 1.0/* defalut 1.0s */;
        $data  = array();
        $processed = 0;

        // start process
        $links = $errors = $reject = array();
        foreach($this->tasks as $key => $task)
        {
            /* useful when async request */
            if(isset($this->tasks[$key]['running']))
            {
                continue;
            }

            $pos = strpos(strtolower(trim($task['c']->input_sql('')['sql'])), 'select');
            if($pos !== FALSE && $pos == 0)
            {
                $db = $this->mysqlc->fetch($task['config_key'], $task['config'], TRUE);
            }
            else
            {
                $db = $this->mysqlc->fetch($task['config_key'], $task['config']);
            }

            if(!$db || !$db->ping())
            {
                $config = empty($task['config']) ? 'default config' : implode("-", $task['config']);
                $error  = !$db ?  'error db handler' : $db->error;
                cy_log(CYE_WARNING, "MySQL connect error(%d) %s %s", $db->errno, $config, $error);

                $data[$key] = ['errno' => $db->errno, 'error' => $error, 'c' => $task['c']];
                $task['c']->callback($data[$key]);

                unset($this->tasks[$key]);
                $processed++;
                continue;
            }

            /* overload mysql read timeout when $options['timeout'] is set. */
            if(isset($options['timeout']))
            {
                $db->options(MYSQLI_OPT_READ_TIMEOUT, $options['timeout']);
            }

            $query = $task['c']->inputs(['db' => $db]);
            if($query && !empty($query['sql']))
            {
                $db->query($query['sql'], MYSQLI_ASYNC);
                $db->setkey($key);
                $this->hd[$key] = $db;
                $this->tasks[$key]['running'] = 1;
            }
            else
            {
                $data[$key] = ['errno' => CYE_PARAM_ERROR, 'error' => "sql statment error"];
                $task['c']->callback($data[$key]);

                $this->mysqlc->restore($this->tasks[$key]['config_key'], $db);
                unset($this->tasks[$key]);
                $processed++;
            }
        }

        if(empty($this->hd))
        {
            return ['errno' => 0, 'data' => $data, 'processed' => $processed, 'message' => 'empty hd'];
        }

        do
        {
            $links = $errors = $reject = $this->hd;
            if(!mysqli_poll($links, $errors, $reject, 0, 100000/* 100ms */)) // timeout 100ms.
            {
                /* timeout */
                goto one_loop;
            }

            foreach($links as $link)
            {
                $key = $link->getkey();
                $c   = $this->tasks[$key]['c'];
                if($result = $link->reap_async_query())
                {
                    if (is_object($result))
                    {
                        // select/ show /...
                        $rows = $result->fetch_all(MYSQLI_ASSOC);
                        $data[$key] = ['errno' => 0, 'data' => $c->outputs($rows)];
                        mysqli_free_result($result);
                    }
                    else
                    {
                        // create/drop/update/insert/delete.
                        $r = ['affected_rows' => $link->affected_rows, 'insert_id' => $link->insert_id];
                        $data[$key] = ['errno' => 0, 'data' => $r, 'info' => $link->info];
                    }
                }
                else
                {
                    // error.
                    cy_log(CYE_ERROR, 'mysql reap_async_query error! [%d] [%s]', $link->errno, $link->error);
                    $data[$key] = ['errno' => $link->errno, 'error' => $link->error];
                }

                $c->callback($data[$key]);

                $this->mysqlc->restore($this->tasks[$key]['config_key'], $link);
                unset($this->hd[$key], $this->tasks[$key]);
                $processed++;
            }

            foreach($errors as $link)
            {
                $key = $link->getkey();
                $data[$key] = ['errno' => $link->errno, 'error' => $link->error];
                $this->tasks[$key]['c']->callback($data[$key]);
                $link->close();
                unset($this->hd[$key], $this->tasks[$key]);
                $processed++;
            }

            foreach($reject as $link)
            {
                /* never reached here. */
                $key = $link->getkey();
                $data[$key] = ['errno' => $link->errno, 'error' => $link->error];
                $this->tasks[$key]['c']->callback($data[$key]);
                $link->close();
                unset($this->hd[$key], $this->tasks[$key]);
                $processed++;
            }

one_loop:
            $t2 = microtime(true);
        }
        while(!empty($this->hd) && $t2 - $t1 < $cycle);

        if(!isset($options['async']))
        {
            foreach($this->hd as $key => $link)
            {
                cy_log(CYE_ERROR, 'mysql query timeout %s %s', $link->host_info, $link->error);
                $data[$key] = ['errno' => CYE_NET_TIMEOUT, 'timeout'];
                $this->tasks[$key]['c']->callback($data[$key]);
                //$link->close();
                $this->mysqlc->restore($this->tasks[$key]['config_key'], $link);
                unset($this->hd[$key], $this->tasks[$key]);
            }
            // end process.
            $cost = (microtime(true) - $t1)*1000000;
            cy_stat('mysql-multi-query', $cost);
        }
        return array('errno' => 0, 'data' => $data, 'processed' => $processed);
    }

    function escape_string($str)
    {
        return addslashes($str);
    }

    function query($sql, $config = NULL)
    {
        $c = new CY_Driver_DB_Default('', ['sql' => $sql], ['method' => 'sql']);
        if($config == NULL && isset($_ENV['config']['db']['slave']))
        {
            $config = $_ENV['config']['db']['slave'][array_rand($_ENV['config']['db']['slave'])];
        }
        $this->add('default', $c, $config);
        $dt = $this->mGet();
        if($dt['errno'] === 0 && isset($dt['data']['default']))
        {
            return $dt['data']['default'];
        }

        return $dt;
    }
    
    function delete($table, $cond = '')
    {
        $sql = "DELETE FROM `".$table."` WHERE ".$cond;
        return $this->query($sql, $_ENV['config']['db']['master']);
    }

    function insert($table, $data, $cond = '')
    {
        $where = array();
        foreach($data as $k => $v)
        {
            if(strpos($v, 'POINT') !== FALSE || strpos($v, 'LINESTRING') !== FALSE)
            {
                $where[] = '`'.$k.'`=GeomFromText(\''.$v.'\')';
            }
            else
            {
                $where[] = '`'.$k.'`=\''.addslashes($v).'\'';
            }
        }
        $sql = "INSERT INTO `".$table."` SET ".implode(',', $where).' '.$cond;

	    $config = isset($_ENV['config']['db']['master']) ? $_ENV['config']['db']['master'] : NULL;
        return $this->query($sql, $config);
    }

    function update($table, $data, $cond = '')
    {
        $where = array();
        foreach($data as $k => $v)
        {
            if(strpos($v, 'POINT') !== FALSE || strpos($v, 'LINESTRING') !== FALSE)
            {
                $where[] = '`'.$k.'`=GeomFromText(\''.$v.'\')';
            }
            else
            {
                $where[] = '`'.$k.'`=\''.addslashes($v).'\'';
            }
        }

        $sql = "UPDATE `".$table."` SET ".implode(',', $where).' WHERE '.$cond;
	$config = isset($_ENV['config']['db']['master']) ? $_ENV['config']['db']['master'] : NULL;
        return $this->query($sql, $config);
    }

    function get($table, $data)
    {
        $where = array();
        foreach($data as $k => $v)
        {
            if(strpos($v, 'POINT') !== FALSE || strpos($v, 'LINESTRING') !== FALSE)
            {
                $where[] = '`'.$k.'`=GeomFromText(\''.$v.'\')';
            }
            else
            {
                $where[] = '`'.$k.'`=\''.addslashes($v).'\'';
            }
        }

	$sql = "SELECT * FROM `".$table."` WHERE ".implode(' AND ', $where);
        return $this->query($sql);
    }


}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
