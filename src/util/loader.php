<?php

class CY_Util_Loader
{
	protected $route;

	public function __construct($auto_register = true)
	{
		if($auto_register)
		{
			spl_autoload_register(array($this, 'loader'));
		}

		$this->route = [
			'cy' => CY_LIB_PATH,
			'ch' => CY_HOME 
				];
	}

	public function loader($name)
	{
		$name = strtolower($name);	
		$head = substr($name, 0, 2);
		if(empty($this->route[$head]))
		{
			return;
		}

		$path = $this->route[$head];

		$filename = $path.'/'.strtr(substr($name, 3), '_', '/').'.php';
		if(is_file($filename))
		{
			include $filename;
		}
	}

}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
