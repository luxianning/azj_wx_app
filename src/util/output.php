<?php

class CY_Util_Output
{
	protected $options = array('display' => 'html');
	protected $data    = array();

	function __construct($data = NULL, $options = NULL)
	{
		$options && $this->options = $options;
		$data    && $this->data    = $data;

		isset($_ENV['display']) && $this->options['display'] = $_ENV['display'];
	}

	function assign()
	{
		switch(func_num_args())
		{
			case 1:
				$this->data += func_get_arg(0);
				break;
			case 2:
				$this->data[func_get_arg(0)] = func_get_arg(1);
				break;
		}
	}

	function render($file = NULL)
	{
		echo $this->get($file); 
	}

	function get($file = NULL)
	{
		if(isset($this->data['backtrace']))
		{
			unset($this->data['backtrace']);
		}

		switch($this->options['display'])
		{
			case 'html':
				$tpl = new CY_Util_Smarty();
				$tpl->assign($this->data);
/*
echo $file,"\n";
var_dump($tpl->fetch($file));
echo "xxi----xxx\n";
*/
				return $tpl->fetch($file);

			case 'jsonh':
				// TODO: use smart here.
				break;


			case 'json':
				if(PHP_SAPI !== 'cli') header('Content-Type: application/json');
				return json_encode($this->data);

			case 'jsonp':
				//header('Content-Type: application/jsonp');
				$method = isset($_GET['method']) ? trim($_GET['method']) : 'callback';
				if(!preg_match('/^[0-9a-zA-Z_]+$/', $method))
				{
					$method = 'callback';
				}

				return $method."(".json_encode($this->data).");";

			case 'raw':
				if(empty($this->data['data']))
				{
					return '';
				}

				return $this->data['data'];

			case 'xml':
			default:
				break;

		}

		return '';
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
