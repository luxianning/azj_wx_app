<?php

class CY_Util_Tools {
    public static function jsonEncodeEx($arr) {
        if (version_compare(PHP_VERSION, "5.4") >= 0) {
            return json_encode($arr, JSON_UNESCAPED_UNICODE);
        } else {
            return urldecode(json_encode(self::urlEncodeEx($arr)));
        }
    }

    public static function urlEncodeEx($str) {
        if (is_array($str)) {
            foreach ($str as $key => $value) {
                $str[urlencode($key)] = self::urlEncodeEx($value);
            }
        } else {
            $str = urlencode($str);
        }
        return $str;
    }

    public static function post($url, $data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }
}
