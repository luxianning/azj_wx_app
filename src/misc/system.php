<?php
/**
 * 一些必需的杂项函数, 这里只放最最基本
 *
 * 这里尽量不要放跟业务相关的东西
 *
 * @file functions.php
 * @author Jianyu Yang <xiaoyjy@gmail.com>
 * @date 2012/12/21 10:31:44
 * @version $Revision: 1.0 $ 
 *  
 */



/**
 * cy_debug_enable  是否将错误信息展现在页面上 
 *
 * @author xiaoyjy@gmail.com
 * @since 1.0
 *
 * @param bool $enable 是否打开错鋘信息
 */
function cy_debug_enable($enable = false)
{
	if($enable)
	{
		ini_set('display_errors' , 'on');
		ini_set('html_errors'    , 'on');
		ini_set('error_reporting', E_ALL);
	}
	else
	{
		ini_set('display_errors', 'off');
	}
}

/**
 * 框架在退出的时间会调用此方法
 *
 * @author xiaoyjy@gmail.com
 */
function cy_shutdown_callback()
{
	$errno = isset($_ENV['errno']) ? $_ENV['errno'] : 0;

	cy_stat_flush();
}

/**
 * 如果脚本中出现异常 ，但未被catch，调用此方法出系统繁忙
 *
 * @param Exception $e 异常堆栈
 */
function cy_exception_handler($e)
{

}

/**
 * 判断一个IP是否为内网IP
 *
 * @param [string|int] $ipv4_addr xxx.xxx.xxx.xxx liked ip address or integer ip.
 * @return bool 是否为内网IP
 */
function cy_reserved_ipv4($ipv4_addr)
{
	$ipn = is_numeric($ipv4_addr) ? (int)$ipv4_addr : ip2long($ipv4_addr);
	$ipb = ($ipn & 0xFFFF0000);
	return (
		(($ipn & 0xFF000000) == 0x0A000000)        || /* 10.0.0.0/8 */
		($ipb <= 0xAC1F0000 && 0xAC100000 <= $ipb) || /* 172.16-32.0.0/16 */
		($ipb == 0xC0A80000)                       || /* 192.168.0.0/16 */
		($ipb == 0x7F000000)                          /* 127.0.0.0/16 */
	);
}

/**
 * Get remote IP addr
 *
 * @return string xxx.xxx.xxx.xxx liked ip address
 */
function cy_client_ip()
{
	(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $ip = $_SERVER['HTTP_X_FORWARDED_FOR']) ||
	(isset($_SERVER['REMOTE_ADDR']         ) && $ip = $_SERVER['REMOTE_ADDR']         ) ||
	(isset($_SERVER['HTTP_CLIENT_IP']      ) && $ip = $_SERVER['HTTP_CLIENT_IP']      ) || $ip = '127.0.0.1';

	return $ip;
}

/**
 * 脚本执行终止, 跟据请求的不同，输入json,xml,html等不同格式的错误信息
 *
 * @author xiaoyjy@gmail.com
 *
 * @param int $errno error code int lib/util/errno.php
 * @param string $message user defined error message
 * @param MP_Util_Output $t output template
 */
function cy_exit($errno = 0, $message = NULL, $t = NULL)
{
	/* Many be used in shutdown functions. */
	$_ENV['errno'] = $errno;

	/* First field must be 'result'. */
	$data = array(
			'errno'   => $errno,
			'message' => empty($message) ? cy_strerror($errno) : $message,
		     );

	/* hacked, return data by message. */
	if(is_array($message))
	{
		$tmp = $message;
		$message = isset($message['error']) ? $message['error'] : NULL;

		unset($tmp['error']);
		$data += $tmp;
	}

	$t == NULL && $t = new MP_Util_Output();
	$t->assign($data);
	echo $t->render();

	/* useful in nginx, useless in apache. */
	if (function_exists('fastcgi_finish_request'))
	{
		/* if use fastcgi, we may finish it first. */
		fastcgi_finish_request();
	}

	exit($errno);
}


/**
 * 将错误码转为错误信息
 *
 * @see /util/errno.php
 * @param int $errno 错误码
 * @return string 错误信息 
 */
function cy_strerror($errno)
{
	/* Can be cached in local memory. */
	if (!isset($_ENV['errors']))
	{
		include MP_LIB_PATH . '/util/error.php';
	}

	return isset($_ENV['errors'][$errno]) ? $_ENV['errors'][$errno] : '';
}

/**
 * 记录接口调用的所花的时间
 *
 * $example cy_stat(__CLASS__.':'.__FUNCTION__, 5000);
 *
 * $param string $key key of interface.
 * $param int $elapdwset_elapsed
 * $param array $options options maybe errno, args.. etc.
 */
function cy_stat($key, $elapsed, $options = array())
{
	if(empty($_ENV['stat'][$key]))
	{
		$_ENV['stat'][$key] = array();
		$_ENV['stat_count'] = 0;
		$_ENV['stat_flush_times'] = 0;
	}

	/* errno === 0 时不写，减少日志大小
	if(empty($options['errno']))
	{
		$options['errno'] = 0;
	}
	*/

	$_ENV['stat_count']++;
	$_ENV['stat'][$key]['c'][] = (int)$elapsed; 
	empty($options) || $_ENV['stat'][$key]['o'][] = $options;
	if($_ENV['stat_count'] === $_ENV['config']['stat_max_line'])
	{
		cy_stat_flush();
	}
}

/**
 * cy_stat_flush
 *
 * 如果$_ENV['stat'], 太大，可以会造成memory leak, 有时间需要手动刷一下。
 */
function cy_stat_flush()
{
	$endtime = microtime(true);
	$elapsed = (int)(($endtime - $_SERVER['REQUEST_TIME_START'])*1000000);
	$post_data = empty($_POST) ? '' : 'input='.json_encode(array_keys($_POST)).' ';
    
    empty($_ENV['stat']) && $_ENV['stat'] = [];
	empty($_ENV['stat_flush_times']) && $_ENV['stat_flush_times'] = 0;
	cy_log(CYE_NOTICE, 'memory:%d %s elapsed={total:%d,detail:%s}', memory_get_usage(), $post_data, $elapsed, json_encode($_ENV['stat']));

	$_ENV['stat'] = array();
	$_ENV['stat_count'] = 0;
	$_ENV['stat_time']  = $endtime;
	$_ENV['stat_flush_times'] += 1;
}

/**
 * cy_guid2bin
 *
 * @param $guid string Microsoft convention (GUID)
 * @return $guid_bin 16bytes binary string.
 */
function cy_guid2bin($guid)
{
	$hex = str_replace('-', '', $guid);
	return pack('H32', $hex);
}

/**
 * cy_bin2guid
 *
 * @param $guid_bin 16bytes binary string.
 */
function cy_bin2guid($guid_bin)
{
	$b1 = substr($guid_bin, 0, 4);
	$b2 = substr($guid_bin, 4, 2);
	$b3 = substr($guid_bin, 6, 2);
	$b4 = substr($guid_bin, 8, 2);
	$b5 = substr($guid_bin, 10);

	return bin2hex($b1).'-'.
		bin2hex($b2).'-'.
		bin2hex($b3).'-'.
		bin2hex($b4).'-'.
		bin2hex($b5);
}

/* just used by cy_array_trim */
function cy_array_filter_r($source, $fn)
{
	$result = array();
	foreach ($source as $key => $value)
	{
		if(is_string($value))
		{
			$value = trim($value);
		}

		if(is_array($value))
		{
			$a = cy_array_filter_r($value, $fn);
			if(!empty($a))
			{
				$result[$key] = $a;
			}

		}
		else if($fn($key, $value))
		{
			$result[$key] = $value; // KEEP
		}
	}

	return $result;
}

function cy_array_trim($source)
{
	$cmp = function($k, $v)
	{
		return !empty($v);
	};

	return cy_array_filter_r($source, $cmp);
}

function cy_empty($val)
{
	return empty($val);
}

function cy_not($val)
{
	return !empty($val);
}

function cy_val($arr, $key, $default = NULL)
{
	return isset($arr[$key]) ? $arr[$key] : $default;
}

/*
function cy_gi($key)
{
	return cy_hs_i_get($key, MP_HS_TYPE_CFG_I);
}

function cy_gs($key)
{
	return cy_hs_s_get($key, MP_HS_TYPE_CFG_S);
}

function cy_si($key, $val)
{
	return cy_hs_i_set($key, MP_HS_TYPE_CFG_I, $val);
}

function cy_ss($key, $val)
{
	return cy_hs_s_set($key, MP_HS_TYPE_CFG_S, $val);
}
*/

function cy_cp(&$dest, $src, $key)
{
	empty($src[$key]) || $dest[$key] = $src[$key];
}

function cy_dt($errno, $data = array(), $options = array())
{       
	if($errno === 0)
	{       
		return ['errno' => $errno, 'data' => $data];
	}       

	$error = empty($data) ? cy_strerror($errno) : $data;
	if(isset($options['trace']))
	{
		$stack = debug_backtrace();
		unset($stack[0]);
		return ['errno' => $errno, 'error' => $error, 'backtrace' => $stack];
	}

	return ['errno' => $errno, 'error' => $error];
} 

function cy_dt_m($dt)
{
	if($dt['errno'] !== 0)
	{
		return $dt;
	}

	$data = array();
	foreach($dt['data'] as $sub)
	{
		if($sub['errno'] !== 0)
		{
			continue;
		}

		$data += $sub['data'];
	}

	return cy_dt(0, $data); 
}

/**
 * calculates intersection of two arrays like array_intersect_key but recursive
 *
 * @param  array/mixed  master array
 * @param  array        array that has the keys which should be kept in the master array
 * @return array/mixed  cleand master array
 */
function cy_intersect_key($master, $mask)
{
	if (!is_array($master))
	{
		return $master;
	}

	foreach ($master as $k=>$v)
	{
		if (!isset($mask[$k]))
		{
			// remove value from $master if the key is not present in $mask
			unset ($master[$k]);
			continue;
		}

		if (is_array($mask[$k]))
		{
			// recurse when mask is an array
			$master[$k] = cy_intersect_key($master[$k], $mask[$k]);
		}

		// else simply keep value
	}

	return $master;
}

function cy_xml2array($string)
{
	$obj = (array)simplexml_load_string($string, 'SimpleXMLElement', LIBXML_NOCDATA|LIBXML_NOBLANKS); 
	return simplexml2array($obj);
}

function cy_simplexml2array($obj)
{
	if(empty($obj)) 
	{
		return '';
	}

	$data = array();
	foreach( $obj as $key => $val )
	{
		if((is_array($val) || is_object($val)))
		{
			$data[$key] = simplexml2array((array)$val );
		}
		else
		{
			$data[$key] = (string)$val;
		}
	}

	return $data;
}

function cy_get_model($table)
{
	return new CH_Model_Default($table);
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
