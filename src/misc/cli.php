<?php

function read_line($o)
{
	static $fp = NULL;
	if($fp === NULL)
	{
		$fp = fopen('php://stdin', 'r');
	}

	$i  = 0;

	if(feof($fp))
	{
		if(isset($o['d']))
		{
			echo "Sleeping ...\n";
			sleep(2);
			continue;
		}
		else
		{
			return '';
		}
	}

	return trim(fgets($fp)); 
}

?>
