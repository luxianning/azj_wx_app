<?php

$_ENV['config']['site'] = 'http://wx-app.cheyou360.com'; // H5站网址
$_ENV['config']['imgsite'] = 'http://wx-upload.cheyou360.com'; // H5站网址
$_ENV['config']['uploadpath'] = "/data/www/wx/wx-upload";

#$_ENV['config']['wx_appid'] = 'wx1b90da89cbfa8f99';
#$_ENV['config']['wx_key'  ] = '9fcd146149a8dc5a2a7aefc4cc0ba606';
#Pine test
#$_ENV['config']['wx_appid'] = 'wx380fac7a50a66dae';
#$_ENV['config']['wx_key'  ] = '02f4ab048fef60345bef663d60c18521';
# 微营销控
#$_ENV['config']['wx_appid'] = 'wx0417c63d142bc571';
#$_ENV['config']['wx_key'  ] = 'dc058b60c5f5376563420df83773d598';
# 爱自驾联盟
$_ENV['config']['wx_appid'] = 'wxd402f16cfd2f3f0e';
$_ENV['config']['wx_key'  ] = '7ad5afddd780c4439bdde3cc439a7296';

/***/
$_ENV['config']['log'] = array
(
 'path'  => dirname(CY_LIB_PATH).'/log',
 'name'  => 'cheyou360',
 'level' => 8,
 'fflush' => false 
);

$_ENV['config']['stat_max_line'] = 256;
$_ENV['config']['xhprof_enable'] = 0;

$_ENV['config']['timeout'] = array
(
 'redis_connect' => 0.2,
 'redis_read' => 0.4,

 'mysql_connect' => 1,
 'mysql_read' => 1,

 'http_connect' => 0.2,
 'http_read' => 0.5,

 'net_default' => 0.6,
);

?>
