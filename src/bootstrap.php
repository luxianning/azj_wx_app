<?php
/**
 * 微信公众平台 PHP SDK 示例文件
 *
 * @author NetPuter <netputer@gmail.com>
 */

include CY_LIB_PATH.'/3rd/wechat.php';

/**
 * 微信公众平台演示类
 */

class CY_Bootstrap extends Wechat {

	/**
	 * 用户关注时触发，回复「欢迎关注」
	 *
	 * @return void
	 */
	protected function onSubscribe() {
        // 关注用户初始化
        $um = new CY_Model_Users();
        $fromUser = $this->getRequest('fromusername');
        $inviteUser = '';
        $qrscene = $this->getRequest('EventKey');
        $sceneId = 0;
        if (strrpos($qrscene, 'qrscene_') !== false) { // 找到
            $sceneId = substr($qrscene, strrpos($qrscene, 'qrscene_') + 8);
        }
        $ret = $um->updateUserInfo($fromUser, $inviteUser);
        // 记录来源信息
        if ($sceneId > 0 || strlen($sceneId) > 1) {
            $channel = new CY_Model_Channel();
            $channel->addSubscribe($sceneId, $fromUser);
        }
        if ($sceneId <= 0) {
            //$ret = $um->updateUserInfo($fromUser, $inviteUser);
		    //$this->responseText('欢迎你！');
            $wm = new CY_Model_WX();
            $data = array();
            $data['media_id'] = "HG6iJ_ThjV0QBKPkq9bSPFfHIZmvIV5IJoGhMQvro-w";
            $ret = $wm->getMediaInfo(CY_Util_Tools::jsonEncodeEx($data));
            if ($ret['errno'] == 0 && !empty($ret['data']) && !empty($ret['data']['news_item'])) {
		        //$this->responseText('收到了文字消息：' . $run);
                $mediaInfo = $ret['data']['news_item'][0];
                $items[] = new NewsResponseItem($mediaInfo['title'], $mediaInfo['digest'], $mediaInfo['thumb_url'], $mediaInfo['url']);
                $this->responseNews($items);
            }
        } else {
            $update = 0; // 是否已经更新用户信息
            $am = new CY_Model_Activity();
            //$r = $um->get(['openid' => $fromUser]);
            //if ($r['errno'] !== 0 || empty($r['data'])) {
            //    $ret = $um->updateUserInfo($fromUser, $inviteUser);
            //    $uid = $ret['user']['id'];
            //    $update = 1;
            //} else {
            //    $uid = $r['data'][0]['id'];
            //}
            $uid = $ret['user']['id'];
            if ($sceneId > 10000000) { // 带了邀请参数
                $r2 = $am->getActivityUser($sceneId - 10000000);
                $activityUser = $r2['data'][0];
                $inviteUser = $activityUser['openid'];
                $help = $am->help($activityUser['activity_id'], $sceneId - 10000000, $uid, $ret['new']);
                $am->sendHelpSuccMessage($activityUser['activity_id'], $sceneId - 10000000, $fromUser);
            } elseif ($sceneId > 0) {
                $r3 = $am->join($sceneId, $uid, $fromUser);
                $am->sendJoinSuccMessage($sceneId, $r3['data'], $fromUser);
            }
        }
	}

	/**
	 * 用户已关注时,扫描带参数二维码时触发，回复二维码的EventKey (测试帐号似乎不能触发)
	 *
	 * @return void
	 */
	protected function onScan()
	{
		//$this->responseText('二维码的EventKey：' . $this->getRequest('EventKey'));
		//$app = new CH_App_1_Scan();
		//$app->init($this);
		//$app->run($this->getRequest('EventKey'), $this);
        //$fromUser = $this->getRequest('fromusername');
        //$qrscene = $this->getRequest('EventKey');
        //$sceneId = 0;
        //if (strrpos($qrscene, 'qrscene_') !== false) { // 找到
        //    $sceneId = substr($qrscene, strrpos($qrscene, 'qrscene_') + 8);
        //}
        //if ($sceneId > 0) {
        //    $channel = new CY_Model_Channel();
        //    $channel->addScan($sceneId, $fromUser);
        //}
        //if ($fromUser == 'oR73FwLTDEwIWzRMZPFZD2hqY4KA') {
		//    $this->responseText('二维码的EventKey：' . $this->getRequest('EventKey'));
        //}
	}

	/**
	 * 用户取消关注时触发
	 *
	 * @return void
	 */
	protected function onUnsubscribe() {
		// 「悄悄的我走了，正如我悄悄的来；我挥一挥衣袖，不带走一片云彩。」
        $um = new CY_Model_Users();
        $fromUser = $this->getRequest('fromusername');
        $data['subscribe'] = 0;
        $um->update(['openid' => $fromUser], $data);
	}

	/**
	 * 上报地理位置时触发,回复收到的地理位置
	 *
	 * @return void
	 */
	protected function onEventLocation() {
		//$this->responseText('收到了位置推送：' . $this->getRequest('Latitude') . ',' . $this->getRequest('Longitude'));
	}

	/**
	 * 收到文本消息时触发，回复收到的文本消息内容
	 *
	 * @return void
	 */
	protected function onText() {

		$run = $this->getRequest('content');
		$am = new CY_Model_Activity();
        $acts = $am->getActivityByKeyword($run);
        if (is_array($acts) && !empty($acts)) {
            $items = array();
            foreach ($acts as $item) {
                $items[] = new NewsResponseItem($item['title'], $item['desc'], $_ENV['config']['imgsite'].$item['picurl'], $item['url']);
            }
            $this->responseNews($items);
        } else {
            // do nothing
		    //$this->responseText('收到了文字消息：' . $run);
            if ($run == "回复A") {
                $wm = new CY_Model_WX();
                $data = array();
                $data['media_id'] = "HG6iJ_ThjV0QBKPkq9bSPFfHIZmvIV5IJoGhMQvro-w";
                $ret = $wm->getMediaInfo(CY_Util_Tools::jsonEncodeEx($data));
                if ($ret['errno'] == 0 && !empty($ret['data']) && !empty($ret['data']['news_item'])) {
		            //$this->responseText('收到了文字消息：' . $run);
                    $mediaInfo = $ret['data']['news_item'][0];
                    $items[] = new NewsResponseItem($mediaInfo['title'], $mediaInfo['digest'], $mediaInfo['thumb_url'], $mediaInfo['url']);
                    $this->responseNews($items);
                }
            }
        }
	}

	/**
	 * 收到图片消息时触发，回复由收到的图片组成的图文消息
	 *
	 * @return void
	 */
	protected function onImage() {
		/*
		$items = array(
				new NewsResponseItem('标题一', '描述一', $this->getRequest('picurl'), $this->getRequest('picurl')),
				new NewsResponseItem('标题二', '描述二', $this->getRequest('picurl'), $this->getRequest('picurl')),
			      );

		$this->responseNews($items);
		*/
	}

	/**
	 * 收到地理位置消息时触发，回复收到的地理位置
	 *
	 * @return void
	 */
	protected function onLocation() {
		//$num = 1 / 0;
		// 故意触发错误，用于演示调试功能
		//$this->responseText('收到了位置消息：' . $this->getRequest('location_x') . ',' . $this->getRequest('location_y'));
	}

	/**
	 * 收到链接消息时触发，回复收到的链接地址
	 *
	 * @return void
	 */
	protected function onLink() {
		//$this->responseText('收到了链接：' . $this->getRequest('url'));
	}

	/**
	 * 收到语音消息时触发，回复语音识别结果(需要开通语音识别功能)
	 *
	 * @return void
	 */
	protected function onVoice() {
		//$this->responseText('收到了语音消息,识别结果为：' . $this->getRequest('Recognition'));
	}

	/**
	 * 收到自定义菜单消息时触发，回复菜单的EventKey
	 *
	 * @return void
	 */
	protected function onClick()
	{
		//$app = new CH_App_1_Clicks();
		//$run = $this->getRequest('EventKey');

		//$app->init($this);
		//$app->$run($this);
        $eventKey = $this->getRequest('EventKey');
        if ($eventKey == 'ext1') {
            $fromUser = $this->getRequest('fromusername');
            //$am = new CY_Model_Activity();
            //$r = $am->getImageMediaId(27);
            //$wm = new CY_Model_WX();
            //$wm->sendQrcode($r['data'], $fromUser);
            //$r = $am->join(1,12, $fromUser);
        }

		//$this->responseText('你点击了菜单：' . $eventKey);
	}

	/**
	 * 收到未知类型消息时触发，回复收到的消息类型
	 *
	 * @return void
	 */
	protected function onUnknown() {
		//$this->responseText('收到了未知类型消息：' . $this->getRequest('msgtype'));
	}

}

