<?php
/**
  * wechat php test
  */

error_reporting(0);
  
$dbconfig = require_once dirname(__FILE__).'/../mysqlconf.php';
require_once dirname(__FILE__).'/../infoconfig.php';

 
$cfg['tb_pre'] = $dbconfig['DB_PREFIX'];
$cfg['db_charset'] = 'utf8'; 
$cfg['sqlerr'] = '1';
$cfg['errlog'] = '1';
$cfg['timediff'] = '0'; 
$fr_time = time();
define('FR_ROOT', str_replace("\\", '/', dirname(__FILE__)));
define('CACHE_ROOT', $cfg['cache_dir'] ? $cfg['cache_dir'] : FR_ROOT.'/cache');
define('DATA_ROOT', FR_ROOT.'/data');
include('mysql.class.php');
$db = new db_mysql();
$db->halt = $cfg['sqlerr'];
$db->connect($dbconfig['DB_HOST'], $dbconfig['DB_USER'], $dbconfig['DB_PWD'], $dbconfig['DB_NAME'],0);


$tmp = $_SERVER['PHP_SELF'];
$tmp = str_replace("weixin.php", "", $tmp);
  
	
//define your token
define("TOKEN", $infoconfig['wxid']);
$wechatObj = new wechatCallbackapiTest();
$wechatObj->valid();
$wechatObj->responseMsg();


class wechatCallbackapiTest
{
	public function valid()
    {
        $echoStr = $_GET["echostr"];
        //valid signature , option
        if($this->checkSignature()){
        	echo $echoStr;
        	//exit;
        }
    }

    public function responseMsg()
    {
		global $db,$cfg,$tmp,$dbconfig,$infoconfig;
		
		
		$domain = $_SERVER['SERVER_NAME'];		

		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

      	//extract post data
		if (!empty($postStr)){
                
              	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
                $fromUsername = $postObj->FromUserName;
                $toUsername = $postObj->ToUserName;
                $keyword = trim($postObj->Content);
				$type=$postObj->MsgType;
				$eventKey=$postObj->EventKey; //菜单点击值
                $time = time();
                $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
                //关注回复
				$eventTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[event]]></MsgType>
							<Event><![CDATA[subscribe]]></Event>
							</xml>";
							
				switch ($type){
				
					case "event";   //关注回复

						//insert wxuser
						$rs = $db->get_one("select id from {$cfg['tb_pre']}userinfo where wecha_id = '".$fromUsername."' ");
						if(!$rs){
							$db->query("INSERT INTO {$cfg['tb_pre']}userinfo(token,wecha_id) VALUES('".$infoconfig['wxid']."','".$fromUsername."')");
						}


						$_SESSION['token'] = $infoconfig['wxid'];
						$_SESSION['wecha_id'] = $fromUsername;
						$msgType = "text";
						$tmp2 = ''; 
						$num = 0;


						$tmp3 = '';
						if($eventKey!=''){ //菜单点击值
							$rs1 = '';
							$tmp3 = " and keyword='".$eventKey."'";
						}

						$rs1 = $db->get_one("select id,typeid,title,picurl,jumpUrl,description from {$cfg['tb_pre']}reply where keyword='' "); 
						if($rs1){
							   if($rs1['typeid'] == "1"){
							   	  $content = $rs1['description'];	
							   }elseif ($rs1['typeid'] == "2") {
							   	    $num++;
									$tmp2 .="<item>
									<Title><![CDATA[".$rs1['title']."]]></Title>
									<Description><![CDATA[".strip_tags($rs1['description'])."]]></Description>
									<PicUrl><![CDATA[".$rs1['picurl']."]]></PicUrl>
									<Url><![CDATA[".$rs1['jumpUrl']."]]></Url>
									</item>";
							    }								
						}else{
							$sql_a = "select id,title,picurl,info,wx_info from {$cfg['tb_pre']}vote WHERE status=1 AND statdate<".$time." AND enddate>".$time." {$tmp3} ORDER BY id DESC LIMIT 0,6 ";
							$query = $db->query( $sql_a );
							while ( $rs = $db->fetch_array( $query ) ){
								$num++;
								if(empty($rs['picurl'])){
									$rs['picurl'] = "http://".$domain.'/Public/images/weixin.jpg';		
								}											
								$url = "http://".$domain."/Home/index.php?g=Home&m=Index&a=index&id=".$rs['id']."&wecha_id=".$fromUsername."&token=".TOKEN;
								$tmp2 .="<item>
										 <Title><![CDATA[".$rs['title']."]]></Title> 
										 <Description><![CDATA[".strip_tags($rs['wx_info'])."]]></Description>
										 <PicUrl><![CDATA[".$rs['picurl']."]]></PicUrl>
										 <Url><![CDATA[".$url."]]></Url>
										 </item>";
										
							}
						}


						$newsTpl="<xml>
									<ToUserName><![CDATA[".$fromUsername."]]></ToUserName>
									<FromUserName><![CDATA[".$toUsername."]]></FromUserName>
									<CreateTime>".$time."</CreateTime>
									<MsgType><![CDATA[news]]></MsgType>
									<ArticleCount>".$num."</ArticleCount>
									<Articles>
									 ".$tmp2."
									</Articles>
									<FuncFlag>0</FuncFlag>
									</xml>";						

						if($num == 0){
							$content = empty($content)?"谢谢关注!":$content;
						}else{
								echo $newsTpl;
								exit;							
						}
						
						$contentStr = $content;
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
						
					break;
					case "text";

						if(!empty($keyword)){

							$rs = $db->get_one("select id from {$cfg['tb_pre']}userinfo where wecha_id = '".$fromUsername."' ");
							if(!$rs){
								$db->query("INSERT INTO {$cfg['tb_pre']}userinfo(token,wecha_id) VALUES('".$infoconfig['wxid']."','".$fromUsername."')");
							}

							$_SESSION['token'] = $infoconfig['wxid'];
							$_SESSION['wecha_id'] = $fromUsername;

							$msgType = "text";
							$tmp2 = ''; 


						$sql_a = "select id,title,picurl,info,wx_info from {$cfg['tb_pre']}vote WHERE status=1 AND statdate<".$time." AND enddate>".$time." and keyword='".$keyword."' ORDER BY id DESC LIMIT 0,1 ";
						$num = 0;

						$query = $db->query( $sql_a );
						while ( $rs = $db->fetch_array( $query ) ){
							$num++;
							if(empty($rs['picurl'])){
								$rs['picurl'] = "http://".$domain.'/Public/images/weixin.jpg';		
							}											
							$url = "http://".$domain."/Home/index.php?g=Home&m=Index&a=index&id=".$rs['id']."&wecha_id=".$fromUsername."&token=".TOKEN;
							$tmp2 .="<item>
									 <Title><![CDATA[".$rs['title']."]]></Title> 
									 <Description><![CDATA[".strip_tags($rs['wx_info'])."]]></Description>
									 <PicUrl><![CDATA[".$rs['picurl']."]]></PicUrl>
									 <Url><![CDATA[".$url."]]></Url>
									 </item>";
									
						}

							if($num == 0){
									//自定义关键词
									$rs1 = $db->get_one("select id,typeid,title,picurl,jumpUrl,description from {$cfg['tb_pre']}reply where keyword='".$keyword."' ");
									if($rs1){
									   
									}else{
										//不匹配	
										$rs1 = $db->get_one("select id,typeid,title,picurl,jumpUrl,description from {$cfg['tb_pre']}reply where keyword='' "); 
									}
									if($rs1){
										   if($rs1['typeid'] == "1"){
										   	  $content = $rs1['description'];	
										   }elseif ($rs1['typeid'] == "2") {
										   	    $num++;
												$tmp2 .="<item>
												<Title><![CDATA[".$rs1['title']."]]></Title>
												<Description><![CDATA[".strip_tags($rs1['description'])."]]></Description>
												<PicUrl><![CDATA[".$rs1['picurl']."]]></PicUrl>
												<Url><![CDATA[".$rs1['jumpUrl']."]]></Url>
												</item>";
										    }	
									}else{
										$content = "谢谢关注！";
									}							



							}

							if(!empty($tmp2)){
							$newsTpl="<xml>
										<ToUserName><![CDATA[".$fromUsername."]]></ToUserName>
										<FromUserName><![CDATA[".$toUsername."]]></FromUserName>
										<CreateTime>".$time."</CreateTime>
										<MsgType><![CDATA[news]]></MsgType>
										<ArticleCount>".$num."</ArticleCount>
										<Articles>
										 ".$tmp2."
										</Articles>
										<FuncFlag>0</FuncFlag>
										</xml>";
										echo $newsTpl;
										exit;	

							}			

						}else{
							$content = "找不到内容!";
						}

						$contentStr = $content;
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					break;				
				}
				echo $resultStr;

        }else {
        	echo "";
        	exit;
        }
    }
		
	private function checkSignature()
	{
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];	
        		
		$token = TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}
}

?>