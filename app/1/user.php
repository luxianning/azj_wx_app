<?php

class CH_App_1_User
{
    protected $um;
    protected $data;

    function __construct()
    {
        $this->um = new CY_Model_Users();
    }

    function __call($method, $args)
    {
        return cy_dt(0);
    }

    function check($id, $req, $env)
    {
        if(empty($id))
        {
            $error = "error: No weixin openid";
            return cy_dt(-1, $error);
        }

again:
        $r = $this->um->get(['openid' => $id]);
        if($r['errno'] !== 0 || empty($r['data'][0]) || empty($r['data'][0]['nickname'])) {
            if(empty($again))
            {
                $this->um->init($id, new CY_Model_WX());
                $again = 1;
                goto again;
            }

            $error = "error: no user info.";
            return cy_dt(-2, $error);
        }

        $this->data = $r['data'][0];
        return cy_dt(0);
    }

    function oauth($id, $req, $env)
    {
        if(empty($req['code']))
        {
            return cy_dt(-1, 'No code.');
        }

        $code = $req['code'];
        $wx   = new CY_Model_WX();
        $r = $wx->oauth($code);
        if($r['errno'] !== 0)
        {
            return $r;
        }


        $u = $r['data'];
        $d = [];
        $d['access_token' ] = $u['access_token' ]; 
        $d['refresh_token'] = $u['refresh_token']; 
        $d['oauth_time'   ] = date('Y-m-d H:i:s');
        $openid                = $u['openid'];
        $rt = $this->um->get(['openid' => $openid]);
        //$updateFlag = true;;
        if ($rt['errno'] == 0 && !empty($rt['data'][0])) {
            //$updateFlag = false;
            unset($d['openid']);
            $ru = $this->um->update(['openid' => $openid], $d);
            if ($rt['data'][0]['subscribe'] != 1) {
                $this->um->updateUserInfo($openid, "");
            }
        } else {
        // 网页端静默认证，关注时候丰富信息
        //if ($rt['errno'] !== 0 || empty($rt['data'][0]) || empty($rt['data'][0]['nickname'])) {
        //    $this->check($openid, $req, $env);
        //}
            $d['openid'] = $openid;
            $ru = $this->um->set($d);
            $this->um->updateUserInfo($openid, "");
        }

        setcookie('wxid', $openid, $_SERVER['REQUEST_TIME']+3600000, '/');
        if(isset($req['ref']))
        {
            header("Location: ".$req['ref']);
            exit;
        }

        return $ru;
    }

}

?>
