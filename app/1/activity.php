<?php

class CH_App_1_Activity
{
    const NUMBER_PER_PAGE = 10;

	protected $um;
	protected $data;

	public function __construct()
	{
		$this->um = new CY_Model_Users();
		$this->am = new CY_Model_Activity();
	}

    public function my() {
        //$id = 115;
        //$ret = $this->am->getImageMediaId($id);
        //var_dump($ret);
        //exit;
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        if ($r['errno'] !== 0 || empty($r['data'])) {
            return cy_dt(-1, '用户不存在！');
        }
        $ret['user'] = $r['data'][0];
        $myJoins = $this->am->getMyJoins($r['data'][0]['id']);
        $ret['myJoins'] = $myJoins;
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        return $ret;
		//return cy_dt(0);
    }

    public function detail($id) {
        $org = isset($_REQUEST['org']) ? $_REQUEST['org'] : 0;
        if ($org != 1) {
            // 判断是否已经参加
            $openid = $_COOKIE['wxid'];
            $r = $this->um->get(['openid' => $openid]);
            if ($r['errno'] == 0 && !empty($r['data'])) {
                $uid = $r['data'][0]['id'];
                $ret = $this->am->checkJoinedByUidAndAid($uid, $id);
                if ($ret['errno'] == 0 && !empty($ret['data'][0])) {
                    $auid = $ret['data'][0]['id'];
                    header("Location: /index.php?m=activity&f=joinSuccess&id=$auid");
                    return cy_dt(0);
                }
            }
        }

        include CY_LIB_PATH."/3rd/jssdk.php";
        $wm = new CY_Model_WX();
        $ticket = $wm->getJsApiTicket();
		$appid = $_ENV['config']['wx_appid'];
		$appkey   = $_ENV['config']['wx_key'  ];
        $jssdk = new JSSDK($appid, $appkey, $ticket);
        $ret['signPackage'] = $jssdk->GetSignPackage();
        $this->am->updateStatis($id);
        $cond = array('id' => $id);
        $activity = $this->am->get($cond);
        $ret['activity'] = $activity['data'][0];
        $ret['activity']['shareUrl'] = $_ENV['config']['site']."/index.php?m=activity&f=detail&id=".$ret['activity']['id'];
        $joins = $this->am->getActivityUsers($id);
        $ret['joins'] = $joins['data'];
        $ret['joinsCnt'] = count($joins['data']);
        $awards = $this->am->getActivityAwards($id);
        $ret['awards'] = $awards['data'];
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        return $ret;
    }

    public function help($id) {
        $r = $this->am->getActivityUser($id);
        if ($r['errno'] != 0) {
            return $r;
        } else {
            $ret['activityUser'] = $r['data'][0];
        }
        $cond = array('id' => $r['data'][0]['activity_id']);
        $activity = $this->am->get($cond);
        $ret['activity'] = $activity['data'][0];
        $helps = $this->am->getHelpUsers($id);
        $ret['helpUsers'] = $helps['data'];
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        return $ret;
    }

    public function helpDo($id) {
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        if ($r['errno'] !== 0 || empty($r['data']) || $r['data'][0]['subscribe'] != 1) {
            $wm = new CY_Model_WX();
            // 对于助力数增加10000000，预留10000000为活动ID
            $qrRet= $wm->qr_tiket($id + 10000000);
            $ticket = $qrRet['data']['ticket'];
            $url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$ticket;
            return cy_dt(-1, $url);
        } else {
            $ar = $this->am->getActivityUser($id);
            if ($ar['errno'] != 0 || empty($ar['data'][0])) {
                return cy_dt(-3, '服务器异常');
            }
            $activityUser = $ar['data'][0];
            if ($activityUser['user_id'] == $r['data'][0]['id']) {
                return cy_dt(-2, '不能自己给自己助力');
            }
            $ret = $this->am->help($activityUser['activity_id'], $id, $r['data'][0]['id']);
            if ($ret['errno'] == 0) {
                $this->am->sendHelpSuccMessage($activityUser['activity_id'], $id, $openid);
                return cy_dt(0, $activityUser['activity_id']);
            } elseif ($ret['errno'] == 1062) {
                return cy_dt(-2, '您已经为他助力过');
            } else {
                return cy_dt(-2, $ret['error']);
            }
        }
    }

    public function latest() {
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        $activity = $this->am->getActivity(1);
        $activityId = $activity[0]['id'];
        $this->am->updateStatis($activityId);
        if ($r['errno'] == 0 && !empty($r['data'][0])) {
            $uid = $r['data'][0]['id'];
            $ret = $this->am->checkJoinedByUidAndAid($uid, $activityId);
            if ($ret['errno'] == 0 && !empty($ret['data'][0])) {
                $auid = $ret['data'][0]['id'];
                header("Location: /index.php?m=activity&f=joinSuccess&id=$auid");
                return cy_dt(0);
            }
        }
        header("Location: /index.php?m=activity&f=detail&id=$activityId");
        return cy_dt(0);
    }

    public function all() {
        $pg = isset($_REQUEST['pg']) ? $_REQUEST['pg'] : 1;
        //$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 0; // 1为beings 2为ends
        $offset = ($pg - 1) * self::NUMBER_PER_PAGE;
        $endsRet = $this->am->getActivity(self::NUMBER_PER_PAGE, $offset, 2);
        $beingsRet = $this->am->getActivity(self::NUMBER_PER_PAGE, $offset, 1);
        $ret['pg'] = $pg;
        $ret['beingsMore'] = count($beingsRet) == self::NUMBER_PER_PAGE ? 1 : 0;
        $ret['endsMore'] = count($endsRet) == self::NUMBER_PER_PAGE ? 1 : 0;
        $ret['beings'] = $beingsRet;
        $ret['ends'] = $endsRet;
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        return $ret;
    }

    public function qrcode() {
        $openid = $_COOKIE['wxid'];
        $user = $this->um->get(['openid' => $openid]);
        $wm = new CY_Model_WX();
        $wm->sendQrcode($user['data'][0]['id'], $openid);
        return cy_dt(0);
    }

    public function join($id) {
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        if ($r['errno'] !== 0 || empty($r['data']) || $r['data'][0]['subscribe'] != 1) {
            $wm = new CY_Model_WX();
            $qrRet= $wm->qr_tiket($id);
            $ticket = $qrRet['data']['ticket'];
            $url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$ticket;
            return cy_dt(-1, $url);
        }
        $r = $this->am->join($id, $r['data'][0]['id'], $openid);
        if ($r['errno'] == 0) { // 发送提醒消息
            //$this->am->getImageMediaId($id);
            $this->am->sendJoinSuccMessage($id, $r['data'], $openid);
            return cy_dt(0, $r['data']);
        } elseif ($r['errno'] == 1062) {
            return cy_dt(-2, '您已经参加过，不能重复参加');
        } else {
            return cy_dt(-3, '参加失败，请重试');
        }
    }

    public function joinSuccess($id) {
        include CY_LIB_PATH."/3rd/jssdk.php";
        $wm = new CY_Model_WX();
        $ticket = $wm->getJsApiTicket();
		$appid = $_ENV['config']['wx_appid'];
		$appkey   = $_ENV['config']['wx_key'  ];
        $jssdk = new JSSDK($appid, $appkey, $ticket);
        $ret['signPackage'] = $jssdk->GetSignPackage();
        $r = $this->am->getActivityUser($id);
        if ($r['errno'] != 0 || empty($r['data'][0])) {
            return cy_dt(-1, '服务器异常');
        } else {
            $ret['activityUser'] = $r['data'][0];
            $ret['activityUser']['imgurlLocal'] = $_ENV['config']['imgsite']."/qrcode".$ret['activityUser']['imgurl'];
        }
        $activityId = $r['data'][0]['activity_id'];
        $cond = array('id' => $activityId);
        $activity = $this->am->get($cond);
        $ret['activity'] = $activity['data'][0];
        $ret['activity']['shareUrl'] = $_ENV['config']['site']."/index.php?m=activity&f=help&id=".$ret['activityUser']['id'];
        $helps = $this->am->getHelpUsers($id);
        $ret['helpUsers'] = $helps['data'];
        $joins = $this->am->getActivityUsers($activityId);
        $ret['joins'] = $joins['data'];
        $ret['joinsCnt'] = count($joins['data']);
        $awards = $this->am->getActivityAwards($activityId);
        $ret['awards'] = $awards['data'];
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        return $ret;
    }

    public function sendCard($id) {
        $openid = $_COOKIE['wxid'];
        $r = $this->am->getImageMediaId($id);
        if ($r['errno'] == 0 && !empty($r['data']['media_id'])) {
            $wm = new CY_Model_WX();
            $ret = $wm->sendQrcode($r['data']['media_id'], $openid);
        }
        return cy_dt(0, array('imgurlLocal' => $r['data']['imgurlLocal'], 'mediaId' => $r['data']['media_id']));
    }

    public function myaward($id) {
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        if ($r['errno'] !== 0 || empty($r['data'])) {
            return cy_dt(-1, '用户不存在！');
        }
        $uid = $r['data'][0]['id'];
        $userAwardsInfo = $this->am->getUserAwardsInfo($id, $uid);
        $ret['imgsite'] = $_ENV['config']['imgsite'];
        if ($userAwardsInfo['errno'] == 0) {
            $ret['userAwardsInfo'] = $userAwardsInfo['data'][0];
            return $ret;
        } else {
            return cy_dt(-1, '不存在页面');
        }
    }

    public function updateUserAwardsInfo($id) {
        $openid = $_COOKIE['wxid'];
        $r = $this->um->get(['openid' => $openid]);
        if ($r['errno'] !== 0 || empty($r['data'])) {
            return cy_dt(-1, '用户不存在！');
        }
        $uid = $r['data'][0]['id'];
        $datas['realname'] = $_REQUEST['realname'];
        $datas['mobile'] = $_REQUEST['mobile'];
        $datas['address'] = $_REQUEST['address'];
        $datas['idno'] = $_REQUEST['idno'];
        $ret = $this->am->updateUserAwardsInfo($id, $uid, $datas);
        return $ret;
    }

    public function menu() {
        $wm = new CY_Model_WX();
        $menu = array(
            "button" => array(
                array(
                    "name" => "精选",
                    "sub_button" => array(
                        array(
                            "type" => "view",
                            "name" => "内容精选",
                            "url" => "http://mp.weixin.qq.com/mp/homepage?__biz=MzA4OTU5NjM4Ng==&hid=6&sn=99eb69d601195d8a282ead9841f49edb#wechat_redirect",
                        ),
                        array(
                            "type" => "view",
                            "name" => "最美季节",
                            "url" => "http://b.eqxiu.com/s/Pllb0V0f",
                        ),
                        array(
                            "type" => "view",
                            "name" => "历史消息",
                            "url" => "http://mp.weixin.qq.com/mp/getmasssendmsg?__biz=MzA4OTU5NjM4Ng==#wechat_webview_type=1&wechat_redirect",
                        )
                    )
                ),
                array(
                    "name" => "活动",
                    "sub_button" => array(
                        array(
                            "type" => "view",
                            "name" => "最新活动",
                            "url" => $_ENV['config']['site']."/index.php?m=activity&f=latest",
                        ),
                        array(
                            "type" => "view",
                            "name" => "活动列表",
                            "url" => $_ENV['config']['site']."/index.php?m=activity&f=all",
                        ),
                        array(
                            "type" => "view",
                            "name" => "我的",
                            "url" => $_ENV['config']['site']."/index.php?m=activity&f=my",
                        ),
                        array(
                            "type" => "view",
                            "name" => "摄影大赛",
                            "url" => "http://wx-vote.cheyou360.com/Home/index.php/Index/index/id/9.html",
                        )
                    )
                ),
                array(
                    "name" => "我们",
                    "sub_button" => array(
                        array(
                            "type" => "view",
                            "name" => "App下载",
                            "url" => "http://www.izijia.cn/app_down/",
                        ),
                        array(
                            "type" => "view",
                            "name" => "『爱自驾』",
                            "url" => "http://mp.weixin.qq.com/s?__biz=MzA4OTU5NjM4Ng==&mid=504750804&idx=1&sn=c23e7323baf5dc56b60fac425bb0216e#rd",
                        ),
                        array(
                            "type" => "view",
                            "name" => "『联盟说』",
                            "url" => "http://mp.weixin.qq.com/s?__biz=MzIzNDE4NDUxOQ==&mid=402592074&idx=1&sn=10e718cff3db00c701126d1c3000e960&scene=18#wechat_redirect",
                        ),
                        array(
                            "type" => "view",
                            "name" => "爱自驾网",
                            "url" => "http://www.izijia.cn",
                        ),
                        array(
                            "type" => "view",
                            "name" => "会员商城",
                            "url" => "https://wap.koudaitong.com/v2/feature/1gtvjdiw1"
                        )
                    )
                )
            )
        );
        $wm->setMenu(CY_Util_Tools::jsonEncodeEx($menu));
		return cy_dt(0);
    }

    // 后台调用设置菜单
    public function setMenu() {
        $menuJson = $_REQUEST['menu'];
        $wm = new CY_Model_WX();
        return $wm->setMenu($menuJson);
	//return cy_dt(0);
    }

    // 后台调用创建新的永久二维码
    public function addChannel()
    {
        $cm = new CY_Model_Channel();
        $name = $_REQUEST['name'];
        $channel = $_REQUEST['channel'];
        return $cm->addChannel($name, $channel);
    }

    // 推送活动$id
    public function pushMessage($id) {
        //$openid = isset($_REQUEST['openid']) ? $_REQUEST['openid'] : $_COOKIE['wxid'];
        $backend = isset($_REQUEST['backend']) ? $_REQUEST['backend'] : 0;
        return $this->am->pushMessage($id, $backend);
    }

    // 推送获奖信息
    public function pushAwardsMessage($id) {
        //$openid = isset($_REQUEST['openid']) ? $_REQUEST['openid'] : $_COOKIE['wxid'];
        $backend = isset($_REQUEST['backend']) ? $_REQUEST['backend'] : 0;
        //file_put_contents("/home/jiansong/id.log", $_COOKIE['wxid'].$id.$openid."\n", FILE_APPEND);
        return $this->am->pushAwardsMessage($id, $backend);
    }

    public function test($id) {
        //$data = array();
        //$data['media_id'] = $id;

        ////$data = array(
        ////    'type' => 'news',
        ////    'offset' => 0,
        ////    'count' => 20
        ////);
        //$wm = new CY_Model_WX();
        //$ret = $wm->getMediaInfo(CY_Util_Tools::jsonEncodeEx($data));

        $openid = $_COOKIE['wxid'];
        $openid = "oR73FwLTDEwIWzRMZPFZD2hqY4KA";
        $r = $this->am->getImageMediaId($id);
        if ($r['errno'] == 0 && !empty($r['data']['media_id'])) {
            $wm = new CY_Model_WX();
            $ret = $wm->sendQrcode($r['data']['media_id'], $openid);
        }

        var_dump($ret);
    }
}
