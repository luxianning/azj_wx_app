<?php

class CH_App_1_Scan
{
	protected $users;
	protected $wx;	
	protected $openid;

	function __construct()
	{
		$this->users = new CY_Model_Users();
		$this->wx    = new CY_Model_WX();
		$this->wx->init();
	}

	function init($event)
	{
		$this->openid = $event->getRequest('fromusername');
		$this->users->init($this->openid, $this->wx);
	}

	function run($id, $event)
	{
        $toUser = $this->users->get(['openid' => $this->openid]);
        $am = new CY_Model_Activity();
        $r = $am->getActivityUser($id);
        $activityUser = $r['data'][0];
        $event->responseText("欢迎你".$toUser['data'][0]['nickname']."，用户".$activityUser['nickname']."邀请了你！");
		return cy_dt(0);
	}

}


?>
