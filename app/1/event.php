<?php

class CH_App_1_Event
{
	protected $em;
	protected $openid;

	function __construct()
	{
		$this->em     = new CY_Model_Events();
		$this->openid = isset($_REQUEST['openid']) ? $_REQUEST['openid'] : '';
		if(empty($this->openid))
		{
			$this->openid = isset($_COOKIE['wxid']) ? $_COOKIE['wxid'] : '';
		}

		if(empty($this->openid))
		{
			exit('No openid.');
		}
	}

	function lists($id, $req, $env)
	{
		$o = [];
		$o['expire'] = isset($req['expire']) ? $req['expire'] : 0;
		$r = $this->em->lists($o);
		return $r;
	}

	function detail($id, $req, $env)
	{
		$r = $this->em->get(['id' => (int)$id]);
		if($r['errno'] !== 0 || !isset($r['data'][0]))
		{
			return cy_dt(0, []);
		}

		$data = $r['data'][0];
		$ru = $this->em->get_enroll($id);
		if($ru['errno'] === 0)
		{
			$data['users'] = $ru['data'];
		}

		$data['exprie'] = ($_SERVER['REQUEST_TIME'] > strtotime($data['enddate']));
		$data['was_joined'] = false;
		foreach($data['users'] as &$u)
		{
			if($u['openid'] === $this->openid)
			{
				$data['was_joined'] = true;
			}

			$u['jointime'] = cy_datetime_friendly($u['jointime']);
		}
		unset($u);	

		$data['openid'] = $this->openid;
		return cy_dt(0, $data);
	}


	function join($id, $req, $env)
	{
		if(empty($this->openid))
		{
			return cy_dt(CYE_PARAM_ERROR, 'No openid.');
		}

		$r = $this->em->get(['id' => (int)$id]);
		if(empty($r['data'][0]))
		{
			return cy_dt(-1, 'No such event');
		}

		return $this->em->join($this->openid, $r['data'][0]);
	}

}


?>
