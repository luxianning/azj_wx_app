<?php

include CY_LIB_PATH.'/3rd/wechat.php';

if(isset($_REQUEST['openid']))
{
	setcookie('wxid', $_REQUEST['openid'], $_SERVER['REQUEST_TIME']+3600000, '/');
    if (!isset($_REQUEST['backend']) || $_REQUEST['backend'] != 1) {
	    $uri = $_SERVER['REQUEST_URI'];
	    $uri = preg_replace('/openid=[^&]+/', '', $uri);
	    header("Location: ".$uri);
	    exit;
    }
}

if(empty($_COOKIE['wxid']) && empty($_REQUEST['openid']) && (!isset($_REQUEST['backend']) || $_REQUEST['backend'] != 1))
{
	if(empty($_GET['_from']))
	{
		$orilink  = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        // By Pine.
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/index.php?m=user&f=oauth&_from=wx&ref='.urlencode($orilink);
		$self_url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$_ENV['config']['wx_appid']
			.'&redirect_uri='.urlencode($url)
			.'&response_type=code&scope=snsapi_base&state=base#wechat_redirect';
			//.'&response_type=code&scope=snsapi_userinfo&state=base#wechat_redirect';

		header("Location: ".$self_url);
		exit("NO openid, permission denied");
	}	
}

