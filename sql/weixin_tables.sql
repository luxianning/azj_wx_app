CREATE TABLE IF NOT EXISTS `tokens` (
    id int(10) unsigned NOT NULL AUTO_INCREMENT,
    token varchar(255) NOT NULL COMMENT 'token',
    ticket varchar(255) NOT NULL COMMENT 'jsapi_ticket',
    create_time int(10) NOT NULL COMMENT '生成时间戳',
    expired_time int(10) NOT NULL COMMENT '失效时间戳',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_token` (`token`),
    KEY `idx_times` (`create_time`,`expired_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='token生命周期';

CREATE TABLE IF NOT EXISTS `user`(
    id bigint(20) unsigned AUTO_INCREMENT,
    unionid varchar(63) NOT NULL COMMENT '不同应用下一样的unionid',
    openid varchar(63) NOT NULL COMMENT '用户的标识',
    nickname varchar(63) NOT NULL COMMENT '用户昵称',
    sex tinyint(4) NOT NULL COMMENT '0未知1男2女',
    subscribe tinyint(4) NOT NULL COMMENT '是否订阅该公众号标识,0未关注,1关注,取消关注或者运营移除时候变更为0',
    city varchar(31) NOT NULL COMMENT '城市',
    country varchar(31) NOT NULL COMMENT '国家',
    province varchar(31) NOT NULL COMMENT '省份',
    headimgurl varchar(255) NOT NULL COMMENT '用户头像,最后一个数值代表正方形大小(0-640*640,46,64,96,132)',
    imglocal varchar(255) NOT NULL COMMENT '本地化用户头像',
    subscribe_time bigint(20) NOT NULL COMMENT '用户关注时间戳,多次关注,取最后',
    remark varchar(31) NOT NULL COMMENT '公众号对粉丝的备注信息',
    groupid int(10) NOT NULL COMMENT '用户所在分组ID',
    invite_openid varchar(63) NOT NULL COMMENT '邀请者openid,第一次写入时候就不再修改,用户记录不做强删除操作',
    email varchar(255) NOT NULL DEFAULT '' COMMENT '用户邮箱,个人信息有设置时候才有',
    mobile varchar(31) NOT NULL DEFAULT '' COMMENT '手机号,个人信息有设置时候才有',
    address varchar(511) NOT NULL DEFAULT '' COMMENT '联系地址,个人信息有设置时候才有',
    access_token varchar(255) NOT NULL COMMENT '网页授权后access_token',
    refresh_token varchar(255) NOT NULL COMMENT '网页授权后refresh_token',
    oauth_time datetime NOT NULL COMMENT '授权时间',
    PRIMARY KEY (id),
    UNIQUE KEY uk_openid(openid),
    KEY idx_invite_openid(invite_openid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息';

CREATE TABLE IF NOT EXISTS `activity`(
    id int(10) NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL COMMENT '活动标题',
    short_title varchar(63) NOT NULL DEFAULT '' COMMENT '活动短标题',
    keyword varchar(31) NOT NULL COMMENT '关键词',
    summary varchar(511) NOT NULL COMMENT '摘要,同时用于公众号推送时候展示的文字信息',
    content varchar(1023) NOT NULL COMMENT '活动内容',
    img varchar(255) NOT NULL COMMENT '活动背景大图',
    thumb varchar(255) NOT NULL COMMENT '推送公众号展示的封面图',
    status tinyint(4) NOT NULL COMMENT '活动状态,0草稿,1已发布,2删除作废',
    push tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否推送到公众号,每月能推送4条,0未推送,1推送,已发布但没推送的可以再推送，未发布的推送直接变发布',
    push_time datetime NOT NULL COMMENT '推送到公众号时间',
    start_time datetime NOT NULL COMMENT '活动开始时间',
    end_time datetime NOT NULL COMMENT '活动结束时间',
    apply_start_time datetime NOT NULL COMMENT '报名活动开始时间',
    apply_end_time datetime NOT NULL COMMENT '报名活动结束时间',
    qrcode varchar(255) NOT NULL COMMENT '活动二维码地址',
    visits int(10) NOT NULL COMMENT '访问次数',
    apply_num int(10) NOT NULL COMMENT '申请参加人数',
    new_fans_num int(10) DEFAULT '0',
    ID_card_status tinyint(4) DEFAULT '0' COMMENT '是否需要身份证信息，0不需要，1需要',
    send_award_status tinyint(4) DEFAULT '0' COMMENT '确认发奖状态，0未发奖，1已发奖',
    share_logo varchar(255) NOT NULL COMMENT '分享的logo',
    share_desc varchar(64) NOT NULL COMMENT '分享的简介',
    invite_img varchar(255) NOT NULL COMMENT '邀请卡背景图',
    create_time datetime NOT NULL COMMENT '活动创建时间',
    update_time datetime NOT NULL COMMENT '活动最后修改时间',
    PRIMARY KEY (id),
    KEY idx_keyword(keyword)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动列表';

CREATE TABLE IF NOT EXISTS `award`(
    id int(10) NOT NULL AUTO_INCREMENT,
    activity_id int(10) NOT NULL COMMENT '活动ID',
    title varchar(31) NOT NULL COMMENT '奖项头衔,一等奖,纪念奖等',
    name varchar(31) NOT NULL COMMENT '奖品名称',
    type tinyint(4) NOT NULL COMMENT '评奖标准,0按人数,1按达标情况',
    total int(10) NOT NULL COMMENT '数量',
    img varchar(255) NOT NULL COMMENT '奖品图片',
    status tinyint(4) NOT NULL COMMENT '状态,0正常,1无效',
    create_time datetime NOT NULL COMMENT '活动创建时间',
    update_time datetime NOT NULL COMMENT '最后修改时间',
    subscribe_num int(10) NOT NULL COMMENT '关注数',
    `describe` varchar(63) NOT NULL COMMENT '奖品描述',
    PRIMARY KEY (id),
    KEY idx_aid(activity_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动奖品';

CREATE TABLE IF NOT EXISTS `activity_user`(
    id int(10) NOT NULL AUTO_INCREMENT,
    activity_id int(10) NOT NULL COMMENT '活动ID',
    user_id bigint(20) NOT NULL COMMENT '用户ID',
    apply_time datetime NOT NULL COMMENT '参加活动时间',
    subscribe int(10) NOT NULL COMMENT '新关注数,之前从未关注过公众号才能被统计到',
    help_times int(10) NOT NULL COMMENT '助力数',
    help_times_new int(10) NOT NULL COMMENT '助力数纠正值',
    qrcode varchar(255) NOT NULL COMMENT '带参数助力页二维码地址,用来邀请助力,需要在用户参加完活动时候与背景图生成合成图片',
    media_id varchar(255) NOT NULL COMMENT '图文上传到公众号基础服务的媒体ID,用户在公众号中发送图片',
    media_expired datetime NOT NULL COMMENT '素材失效时间',
    imgurl varchar(255) NOT NULL COMMENT '本地图片路径',
    user_remark varchar(63) DEFAULT NULL COMMENT '用户备注',
    PRIMARY KEY (id),
    UNIQUE KEY uk_auid(activity_id, user_id),
    KEY idx_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动参加者';

CREATE TABLE IF NOT EXISTS `activity_user_invite`(
    id int(10) NOT NULL AUTO_INCREMENT,
    activity_user_id int(10) NOT NULL COMMENT '参加活动用户关联ID',
    invited_user_id bigint(20) NOT NULL COMMENT '被邀请者ID',
    invite_time datetime NOT NULL COMMENT '邀请时间',
    isnew tinyint(4) NOT NULL COMMENT '是否是新用户',
    PRIMARY KEY (id),
    UNIQUE KEY uk_aui_id(activity_user_id, invited_user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动参加者邀请用户信息';

CREATE TABLE IF NOT EXISTS `activity_user_awards`(
    id int(10) NOT NULL AUTO_INCREMENT,
    activity_user_id int(10) NOT NULL COMMENT '参加活动用户关联ID',
    activity_id int(10) NOT NULL COMMENT '活动ID',
    user_id bigint(20) NOT NULL COMMENT '用户ID',
    award_id int(10) NOT NULL COMMENT '奖品ID',
    realname varchar(63) NOT NULL COMMENT '用户姓名',
    idno varchar(63) NOT NULL DEFAULT '' COMMENT '身份证号',
    email varchar(255) NOT NULL COMMENT '用户邮箱',
    mobile varchar(31) NOT NULL COMMENT '奖品邮寄联系手机号',
    address varchar(511) NOT NULL COMMENT '奖品邮寄地址',
    status tinyint(4) NOT NULL COMMENT '0初始化1已发送推送消息2已提交邮寄信息3已经邮寄',
    create_time datetime NOT NULL COMMENT '获奖时间',
    update_time datetime NOT NULL COMMENT '更新时间，一般是用户补充邮寄信息时间',
    PRIMARY KEY (id),
    UNIQUE KEY uk_auid(activity_id, user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动获奖结果';

CREATE TABLE IF NOT EXISTS `channel_stat`(
    id int(10) NOT NULL AUTO_INCREMENT,
    name varchar(63) NOT NULL DEFAULT '' COMMENT '渠道名字'
    channel varchar(31) NOT NULL COMMENT '二维码参数，渠道key',
    scan bigint(20) UNSIGNED NOT NULL COMMENT '扫描次数',
    subscribe bigint(20) UNSIGNED NOT NULL COMMENT '关注次数',
    ticket varchar(255) NOT NULL DEFAULT '' COMMENT 'ticket',
    url varchar(255) NOT NULL DEFAULT '' COMMENT 'url',
    img varchar(511) NOT NULL DEFAULT '' COMMENT '二维码图片地址',
    PRIMARY KEY (id),
    UNIQUE KEY uk_channel(channel)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='渠道统计汇总数';

CREATE TABLE IF NOT EXISTS `channel_detail`(
    id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    channel varchar(31) NOT NULL COMMENT '二维码参数，渠道key',
    type tinyint(4) NOT NULL DEFAULT 0 COMMENT '0扫描1关注2取消关注',
    openid varchar(63) NOT NULL COMMENT '用户的标识',
    create_time datetime NOT NULL COMMENT '产生时间',
    PRIMARY KEY (id),
    UNIQUE KEY uk_channel(channel, openid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='扫描关注详情';
